package hiring.dtorequests.dtovacancy;

import hiring.dtorequests.DtoParent;

public class DtoSetVacancyActiveInactiveRequest extends DtoParent {
    private String vacancy;

    public String getVacancy() {
        return vacancy;
    }

    public DtoSetVacancyActiveInactiveRequest(String token, String vacancy) {

        super(token);
        this.vacancy = vacancy;
    }
}
