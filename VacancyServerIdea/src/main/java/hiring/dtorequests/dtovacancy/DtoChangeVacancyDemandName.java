package hiring.dtorequests.dtovacancy;

import hiring.dtorequests.DtoParent;

public class DtoChangeVacancyDemandName extends DtoParent {
    private String vacancy, demandName, newDemandName;


    public String getVacancy() {
        return vacancy;
    }

    public String getDemandName() {
        return demandName;
    }

    public String getNewDemandName() {
        return newDemandName;
    }

    public DtoChangeVacancyDemandName(String token, String vacancy, String demandName, String newDemandName) {

        super(token);
        this.vacancy = vacancy;
        this.demandName = demandName;
        this.newDemandName = newDemandName;
    }
}
