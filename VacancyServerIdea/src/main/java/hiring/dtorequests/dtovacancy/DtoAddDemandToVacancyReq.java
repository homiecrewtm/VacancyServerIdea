package hiring.dtorequests.dtovacancy;

import hiring.dtorequests.DtoParent;

public class DtoAddDemandToVacancyReq extends DtoParent {
    private String vacancy, demandName;
    private int demandLevel;
    private boolean isNecessary;


    public String getVacancy() {
        return vacancy;
    }

    public String getDemandName() {
        return demandName;
    }

    public int getDemandLevel() {
        return demandLevel;
    }

    public DtoAddDemandToVacancyReq(String login, String vacancy, String demandName, int demandLevel, boolean isNecessary) {
        super(login);
        this.vacancy = vacancy;
        this.demandName = demandName;
        this.demandLevel = demandLevel;
        this.isNecessary = isNecessary;
    }

    public void setVacancy(String vacancy) {
        this.vacancy = vacancy;

    }

    public String getDemandname() {
        return demandName;
    }

    public void setDemandname(String demandname) {
        this.demandName = demandname;
    }

    public void setDemandLevel(int demandLevel) {
        this.demandLevel = demandLevel;
    }

    public boolean isNecessary() {
        return isNecessary;
    }

    public void setNecessary(boolean necessary) {
        isNecessary = necessary;
    }
}
