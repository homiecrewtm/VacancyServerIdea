package hiring.dtorequests.dtovacancy;

import hiring.dtorequests.DtoParent;

public class DtoReqToChangeNecessaryOofDemand extends DtoParent {
    private String vacancy, demandName;


    public String getVacancy() {
        return vacancy;
    }

    public String getDemandName() {
        return demandName;
    }

    public DtoReqToChangeNecessaryOofDemand(String token, String vacancy, String demandName) {

        super(token);
        this.vacancy = vacancy;
        this.demandName = demandName;
    }
}
