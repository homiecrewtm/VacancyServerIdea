package hiring.dtorequests.dtovacancy;

import hiring.dtorequests.DtoParent;

public class DtoReqToSetNEwLevelToDemand extends DtoParent {
    private String  vacancy, demandName;
    private int demandLevel;

    public DtoReqToSetNEwLevelToDemand(String token, String vacancy, String demandName, int demandLevel) {
        super(token);
        this.vacancy = vacancy;
        this.demandName = demandName;
        this.demandLevel = demandLevel;
    }


    public String getVacancy() {
        return vacancy;
    }

    public String getDemandName() {
        return demandName;
    }

    public int getDemandLevel() {
        return demandLevel;
    }
}
