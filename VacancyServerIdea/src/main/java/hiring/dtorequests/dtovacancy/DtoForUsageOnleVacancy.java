package hiring.dtorequests.dtovacancy;

import hiring.dtorequests.DtoParent;

public class DtoForUsageOnleVacancy extends DtoParent {
    private  String vacancy;

    public String getVacancy() {
        return vacancy;
    }

    public DtoForUsageOnleVacancy(String token, String vacancy) {

        super(token);
        this.vacancy = vacancy;
    }
}
