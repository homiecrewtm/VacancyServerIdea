package hiring.dtorequests;

public class DtoParent {
    private String login;

    public DtoParent(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }
}
