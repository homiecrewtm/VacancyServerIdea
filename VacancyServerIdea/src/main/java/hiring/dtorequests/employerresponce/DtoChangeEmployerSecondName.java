package hiring.dtorequests.employerresponce;

import hiring.dtorequests.DtoParent;

public class DtoChangeEmployerSecondName extends DtoParent {
    private String secondName;

    public String getSecondName() {
        return secondName;
    }

    public DtoChangeEmployerSecondName(String token, String secondName) {

        super(token);
        this.secondName = secondName;
    }
}
