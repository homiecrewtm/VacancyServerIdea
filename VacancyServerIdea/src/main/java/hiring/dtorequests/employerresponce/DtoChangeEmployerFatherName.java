package hiring.dtorequests.employerresponce;

import hiring.dtorequests.DtoParent;

public class DtoChangeEmployerFatherName extends DtoParent {
    private String fatherName;

    public String getFatherName() {
        return fatherName;
    }

    public DtoChangeEmployerFatherName(String token, String fatherName) {

        super(token);
        this.fatherName = fatherName;
    }
}
