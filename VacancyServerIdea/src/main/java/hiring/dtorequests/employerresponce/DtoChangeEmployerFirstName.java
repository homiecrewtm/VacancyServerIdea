package hiring.dtorequests.employerresponce;

import hiring.dtorequests.DtoParent;

public class DtoChangeEmployerFirstName extends DtoParent {
    private String firstName;

    public DtoChangeEmployerFirstName(String login, String firstName) {
        super(login);
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }
}
