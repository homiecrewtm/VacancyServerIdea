package hiring.dtorequests.employerresponce;

import hiring.dtorequests.DtoParent;

public class DtoChangeEmployerPassReq extends DtoParent {
    private String password;

    public DtoChangeEmployerPassReq(String login,String password) {
        super(login);
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
