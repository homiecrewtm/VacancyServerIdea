package hiring.dtorequests.employerresponce;

public class DtoChangeEmployerCompanyName {
    private String login, companyName;

    public String getLogin() {
        return login;
    }

    public String getCompanyName() {
        return companyName;
    }

    public DtoChangeEmployerCompanyName(String login, String companyMane) {

        this.login = login;
        this.companyName = companyMane;
    }
}
