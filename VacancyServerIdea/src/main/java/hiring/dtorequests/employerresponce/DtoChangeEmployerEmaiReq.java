package hiring.dtorequests.employerresponce;

import hiring.dtorequests.DtoParent;

public class DtoChangeEmployerEmaiReq extends DtoParent {
    private String email;

    public String getEmail() {
        return email;
    }

    public DtoChangeEmployerEmaiReq(String token, String email) {

        super(token);
        this.email = email;
    }
}
