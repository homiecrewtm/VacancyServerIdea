package hiring.dtorequests.employerresponce;

import hiring.dtorequests.DtoParent;

public class DtoRequestToAddVacancytoEmployer extends DtoParent {
    private String position, companyName;
    private int salary;
    private boolean isActive;

    public DtoRequestToAddVacancytoEmployer(String login, String position, int salary, String companyName, boolean isActive) {
        super(login);
        this.position = position;
        this.salary = salary;
        this.companyName = companyName;
        this.isActive = isActive;
    }


    public String getPosition() {
        return position;
    }

    public int getSalary() {
        return salary;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
