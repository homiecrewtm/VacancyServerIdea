package hiring.dtorequests.employerresponce;

import hiring.dtorequests.DtoParent;

public class DtoChangeEmployerAddressReq extends DtoParent {

    private String address;



    public String getAddress() {
        return address;
    }

    public DtoChangeEmployerAddressReq(String token, String address) {
        super(token);
        this.address = address;
    }
}
