package hiring.dtorequests.employeeresponse;

import hiring.dtorequests.DtoParent;

public class DtoNewSkillLevelRequest extends DtoParent {
    private String skillName;
    private int level;

    public DtoNewSkillLevelRequest(String token, String skillName, int level) {
        super(token);
        this.skillName = skillName;
        this.level = level;
    }


    public String getDemandName() {
        return skillName;
    }

    public int getLevel() {
        return level;
    }

}
