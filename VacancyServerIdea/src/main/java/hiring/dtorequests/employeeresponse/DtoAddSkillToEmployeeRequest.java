package hiring.dtorequests.employeeresponse;

import hiring.dtorequests.DtoParent;

public class DtoAddSkillToEmployeeRequest extends DtoParent {
    private String skillName;
    private int level;


    public String getDemandName() {
        return skillName;
    }

    public int getLevel() {
        return level;
    }

    public DtoAddSkillToEmployeeRequest(String login, String skillName, int level) {

        super(login);
        this.skillName = skillName;
        this.level = level;
    }
}
