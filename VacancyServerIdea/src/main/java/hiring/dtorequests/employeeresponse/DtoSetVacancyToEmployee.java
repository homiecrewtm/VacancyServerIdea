package hiring.dtorequests.employeeresponse;

public class DtoSetVacancyToEmployee {
    private String employeeToken, employerToken, companyName, vacancy;

    public DtoSetVacancyToEmployee(String employeeToken, String employerToken, String companyName, String vacancy) {
        this.employeeToken = employeeToken;
        this.employerToken = employerToken;
        this.companyName = companyName;
        this.vacancy = vacancy;
    }

    public String getEmployeeToken() {
        return employeeToken;
    }

    public String getEmployerToken() {
        return employerToken;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getVacancy() {
        return vacancy;
    }
}
