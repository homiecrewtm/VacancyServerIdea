package hiring.dtorequests.employeeresponse;

public class DtoVacanciesRequestFOrEmployee {
    private  String login;

    public DtoVacanciesRequestFOrEmployee(String login) {

        this.login = login;
    }

    public String getLogin() {
        return login;
    }
}
