package hiring.dtorequests.employeeresponse;

public class DtoDeleteEmployeeSkillRequest {
    private String token, skillName;

    public String getToken() {
        return token;
    }

    public String getSkillName() {
        return skillName;
    }

    public DtoDeleteEmployeeSkillRequest(String token, String skillName) {

        this.token = token;
        this.skillName = skillName;
    }
}
