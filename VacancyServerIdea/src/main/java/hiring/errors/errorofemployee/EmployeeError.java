package hiring.errors.errorofemployee;

import hiring.errors.errorofemployer.EmployerError;

public class EmployeeError extends EmployerError {

    public EmployeeError(String error) {
        super(error);
    }
}
