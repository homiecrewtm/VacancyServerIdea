package hiring.db;

import hiring.clients.employee.Employee;
import hiring.clients.employer.Employer;
import hiring.demand.Demand;
import hiring.vacancy.Vacancy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class DataBase implements Serializable {

    private static Logger log = LoggerFactory.getLogger(DataBase.class.getName());

    /**
     * String - companyNameEmployers
     */


//    private Set<Employee> activeEmployees = new HashSet<>();

//    private Set<Employee> inactiveEmployees = new HashSet<>();

//    private Set<Employee> allEmployees = new HashSet<>();

//    private Set<Employer> inactiveEmployers = new HashSet<>();

//    private Set<Employer> activeEmployers = new HashSet<>();

    private Set<Demand> skillsDemands = new HashSet<>();

    private Set<Vacancy> activeVacancies = new HashSet<>();

    private Set<Vacancy> inactiveVacancies = new HashSet<>();

    private Connection connection;
    private Statement statement;


    public DataBase() {
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:vacserver");

            statement = connection.createStatement();

            statement.execute("create table if not exists users(token varchar(12) unique not null," +
                    " login varchar(55) not null unique," +
                    " secondName varchar(55) not null, firstName varchar(55) not null,fatherName varchar(55),email TEXT not null unique, isActive boolean not null);");

            statement.execute("create table if not exists secret_data(id integer unique primary key autoincrement," +
                    "login varchar(55) not null unique, password integer(20) unique not null);");

            statement.execute("create table if not exists employers(employer_token varchar(12) unique not null,login varchar(55) not null unique," +
                    " companyName varchar(55) not null, address text not null)");

            statement.execute("create table if not exists employees(employee_token varchar(12) unique not null,login varchar(55) not null unique," +
                    " vacancy_id integer not null unique, skill_id varchar(15))");

            statement.execute("create table if not exists vacancies(vacancy_id varchar(15) unique, name varchar(155) not null," +
                    " isActive boolean,companyName varchar(55) not null,salary float not null,employer_token varchar(12))");

            statement.execute("create table if not exists vacancies_employers(token varchar(12) unique not null, vacancy_id varchar(15) unique)");

            statement.execute("create  table if not exists inactive_employers(id integer not null primary key autoincrement, login varchar(55) not null unique)");

            statement.execute("create table if not exists inactive_employees(id integer not null primary key autoincrement , login varchar(55) not null unique)");

            statement.execute("create table if not exists demands(id_demand integer not null primary key autoincrement,vacancy_id varchar(15)," +
                    " name varchar(55) not null, level integer(5) not null, isNecessary boolean)");

            statement.execute("create table if not exists skills(skill_id varchar(15) not null unique, name varchar(15) not null, level integer not null)");

        } catch (Exception e) {
            log.error(e.getMessage());
        }


    } //     конструктор


//    public Set<Employee> getAllEmployees() {
//        return allEmployees;
//    }
//
//
//    public Set<Employer> getInactiveEmployers() {
//        return inactiveEmployers;
//    }
//
//    public Set<Employer> getActiveEmployers() {
//        return activeEmployers;
//    }
//
//    public Set<Employee> getInactiveEmployees() {
//        return inactiveEmployees;
//    }
//
//    public Set<Employee> getActiveEmployees() {
//        return activeEmployees;
//    }


    public Set<Demand> getSkillsDemands() {
        return skillsDemands;
    }

    public Set<Vacancy> getActiveVacancies() {
        return activeVacancies;
    }

    public Set<Vacancy> getInactiveVacancies() {
        return inactiveVacancies;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    //getters end


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataBase dataBase = (DataBase) o;
        return Objects.equals(skillsDemands, dataBase.skillsDemands) &&
                Objects.equals(activeVacancies, dataBase.activeVacancies) &&
                Objects.equals(inactiveVacancies, dataBase.inactiveVacancies) &&
                Objects.equals(connection, dataBase.connection) &&
                Objects.equals(statement, dataBase.statement);
    }

    @Override
    public int hashCode() {

        return Objects.hash(skillsDemands, activeVacancies, inactiveVacancies, connection, statement);
    }
}


