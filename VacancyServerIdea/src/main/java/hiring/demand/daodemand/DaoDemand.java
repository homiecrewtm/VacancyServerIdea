package hiring.demand.daodemand;

import hiring.clients.employee.daoemployee.daoexceptions.EmployeeDaoException;
import hiring.clients.employer.daoexceptions.EmployerDaoException;
import hiring.demand.Demand;

import java.sql.SQLException;

public interface DaoDemand {
    boolean addDemandToAVacancy(String token, String vacancy, Demand demand) throws EmployerDaoException, SQLException;

    void changeVacancyDemandName(String token, String vacancy, String oldDemandName, String newDemand) throws EmployerDaoException;

    void changeVacancyDemandLevel(String token, String vacancy, String demand, int level) throws EmployerDaoException, EmployeeDaoException;

    void changeVacancyDemandIsNecessary(String token, String vacancy, String demand) throws EmployerDaoException;

}
