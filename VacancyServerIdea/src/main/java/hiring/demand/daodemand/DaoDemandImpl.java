package hiring.demand.daodemand;

import hiring.clients.employee.daoemployee.daoexceptions.EmployeeDaoException;
import hiring.clients.employer.Employer;
import hiring.clients.employer.daoexceptions.EmployerDaoException;
import hiring.db.DataBase;
import hiring.demand.Demand;
import hiring.vacancy.Vacancy;
import org.apache.commons.lang3.StringUtils;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DaoDemandImpl implements DaoDemand {
    private DataBase dataBase;
    private static Logger log = Logger.getLogger(DaoDemandImpl.class.getName());

    public DaoDemandImpl(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    @Override
    public boolean addDemandToAVacancy(String login, String vacancy, Demand demand) throws EmployerDaoException, SQLException {

        if (StringUtils.isEmpty(login) || StringUtils.isEmpty(vacancy) || demand == null) {
            log.log(Level.SEVERE, "Wrong login or vacancy name or there no such vacancy or demand you described is a little bit incorrect");
            throw new IllegalArgumentException("Wrong login or vacancy name or there no such vacancy or demand you described is a little bit incorrect");
        }

        String employer_id = dataBase.getStatement().executeQuery("select employer_token from employers where login='" + login + "';").getString("employer_token");
        String vacancy_id = dataBase.getStatement().executeQuery("select vacancy_id from vacancies where employer_token='" + employer_id + "';").getString("vacancy_id");
        PreparedStatement addToDemands = dataBase.getConnection().prepareStatement("insert into demands(vacancy_id, name, level, isNecessary) values (?,?,?,?)");
        addToDemands.setString(1, vacancy_id);
        addToDemands.setString(2, demand.getDemandName());
        addToDemands.setInt(3, demand.getLevel());
        addToDemands.setBoolean(4, demand.isNecessary());
        addToDemands.execute();

        return true;
    }

    @Override
    public void changeVacancyDemandName(String token, String vacancy, String oldDemandName, String newDemand) throws EmployerDaoException {
//        if (StringUtils.isEmpty(token) || StringUtils.isEmpty(vacancy) || StringUtils.isEmpty(oldDemandName)) {
//            throw new EmployerDaoException("Please check token or vacancy or demand name you want to get");
//        }
//        Optional<Employer> optionalEmployer = dataBase.getEmployers().stream().filter(p -> p.getLogin().equals(token)).findFirst();
//        if (!optionalEmployer.isPresent()) {
//            throw new EmployerDaoException("No User found with ID => " + token);
//        }
//        Employer employer = optionalEmployer.get();
//        Map<String, Vacancy> vacancyMap = employer.getVacancies();
//        vacancyMap.get(vacancy).setDemandName(oldDemandName, newDemand);
    }

    @Override
    public void changeVacancyDemandLevel(String token, String vacancy, String demand, int level) throws EmployerDaoException, EmployeeDaoException {
//        if (StringUtils.isEmpty(token) || StringUtils.isEmpty(vacancy) || StringUtils.isEmpty(demand)) {
//            throw new EmployeeDaoException(String.format("One of the following is invalid: 1)'%s',2)'%s',3)'%s'", token, vacancy, demand));
//        }
//        Optional<Employer> optionalEmployer = dataBase.getEmployers().stream().filter(p -> p.getLogin().equals(token)).findFirst();
//        if (!optionalEmployer.isPresent()) {
//            throw new EmployerDaoException("Please check login or vacancy or demand name you want to get");
//        }
//        Employer employer = optionalEmployer.get();
//        Map<String, Vacancy> vacancyMap = employer.getVacancies();
//        if (vacancyMap.containsKey(vacancy)) {
//            vacancyMap.get(vacancy).setSkillLevel(demand, level);
//            return;
//        }
//        log.log(Level.INFO, "There is no such vacancy");
    }

    @Override
    public void changeVacancyDemandIsNecessary(String token, String vacancy, String demand) throws EmployerDaoException {
//        if (StringUtils.isEmpty(token) || StringUtils.isEmpty(vacancy) || StringUtils.isEmpty(demand)) {
//            throw new EmployerDaoException("Check login or vacancy or name of demand, please");
//        }
//
//        Optional<Employer> optionalEmployer = dataBase.getEmployers().stream().filter(p -> p.getLogin().equals(token)).findFirst();
//        if (!optionalEmployer.isPresent()) return;
//        Employer employer = optionalEmployer.get();
//        Map<String, Vacancy> vacancyMap = employer.getVacancies();
//        if (!vacancyMap.containsKey(vacancy)) return;
//        Vacancy vac = vacancyMap.get(vacancy);
//        if (!vac.getDemands().containsKey(demand)) return;
//        Demand demand1 = vac.getDemands().get(demand);
//        if (!demand1.isNecessary()) {
//            demand1.setNecessary(true);
//            return;
//        }
//        demand1.setNecessary(false);
    }

}
