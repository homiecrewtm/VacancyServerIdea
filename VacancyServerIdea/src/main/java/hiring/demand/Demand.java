package hiring.demand;

import java.util.Objects;

public class Demand {
    private String demandName;
    private int level;
    private boolean isNecessary;

    public Demand(String demandName, int level) {
        this.isNecessary = true;
        this.demandName = demandName;
        setLevel(level);
    }

    public Demand(String demandName, int level, boolean isNecessary) {
        this.demandName = demandName;
        this.level = level;
        this.isNecessary = isNecessary;
    }

    public String getDemandName() {
        return demandName;
    }

    public void setDemandName(String demandName) {
        this.demandName = demandName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level < 1 ? 1 : (level > 5 ? 5 : level);
    }

    public boolean isNecessary() {
        return isNecessary;
    }

    public void setNecessary(boolean necessary) {
        isNecessary = necessary;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Demand demand = (Demand) o;
        return level == demand.level &&
                isNecessary == demand.isNecessary &&
                Objects.equals(demandName, demand.demandName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(demandName, level, isNecessary);
    }

    @Override
    public String toString() {
        return "Demand{" +
                "demandName:" + demandName +
                ", level:" + level +
                ", isNecessary:" + isNecessary +
                '}';
    }
}