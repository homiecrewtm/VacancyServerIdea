package hiring.jsonresponces.employeeresponce;

public class EmployeeResponse {

    private String token;
    private String message;

    public EmployeeResponse(String token, String message) {
        this.token = token;
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
