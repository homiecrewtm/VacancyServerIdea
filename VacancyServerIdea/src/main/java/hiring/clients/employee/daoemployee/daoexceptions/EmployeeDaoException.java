package hiring.clients.employee.daoemployee.daoexceptions;

public class EmployeeDaoException extends Exception {
    public EmployeeDaoException(String s) {
        super(s);
    }
}
