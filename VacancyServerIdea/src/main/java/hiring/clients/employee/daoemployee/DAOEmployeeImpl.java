package hiring.clients.employee.daoemployee;

import hiring.clients.employee.Employee;
import hiring.clients.employee.daoemployee.daoexceptions.EmployeeDaoException;
import hiring.clients.employer.Employer;
import hiring.db.DataBase;
import hiring.demand.Demand;
import hiring.service.jsonservice.CheckJsonService;
import hiring.vacancy.Vacancy;
import org.apache.commons.lang3.StringUtils;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class DAOEmployeeImpl implements DaoEmployee {

    private DataBase dataBase;

    private static Logger log = Logger.getLogger(DAOEmployeeImpl.class.getName());

    public DAOEmployeeImpl(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    @Override
    public boolean insert(Employee employee) throws SQLException {
        if (employee == null) {
            log.log(Level.ALL, "no information about user");
            return false;
        }

        PreparedStatement insertIntoEmployees = dataBase.getConnection().prepareStatement("INSERT INTO employees(employee_token, login, vacancy_id) VALUES(?,?,?)");
        PreparedStatement insertIntoUsers = dataBase.getConnection().prepareStatement("insert into users (token, login, secondName, firstName, fatherName, email, isActive) values (?,?,?,?,?,?,?);");
        PreparedStatement insertSecretData = dataBase.getConnection().prepareStatement("insert into secret_data(login, password) values (?,?);");
        insertIntoEmployees.setString(1, employee.getToken());
        insertIntoEmployees.setString(2, employee.getLogin());
        insertIntoEmployees.setString(3, UUID.randomUUID().toString()); // UUID as identifier

        insertIntoUsers.setString(1, employee.getToken());
        insertIntoUsers.setString(2, employee.getLogin());
        insertIntoUsers.setString(3, employee.getSecondName());
        insertIntoUsers.setString(4, employee.getFirstName());
        insertIntoUsers.setString(5, employee.getFatherName());
        insertIntoUsers.setString(6, employee.getEmail());
        insertIntoUsers.setBoolean(7, employee.isActive());

        insertSecretData.setString(1, employee.getLogin());
        insertSecretData.setInt(2, employee.getPassword().hashCode());

        insertIntoEmployees.execute();
        insertIntoUsers.execute();
        insertSecretData.execute();
        return true;
    }

    @Override
    public boolean delete(String login) throws EmployeeDaoException, SQLException {

        if (StringUtils.isEmpty(login)) {
            throw new IllegalArgumentException("Wrong login");
        }
        PreparedStatement ps = dataBase.getConnection().prepareStatement("delete from users where login=?");
        PreparedStatement preparedStatement = dataBase.getConnection().prepareStatement("delete from employees where login=?");
        PreparedStatement preparedStatement1 = dataBase.getConnection().prepareStatement("delete  from secret_data where login=?");
        preparedStatement1.setString(1, login);
        ps.setString(1, login);
        preparedStatement.setString(1, login);
        ps.execute();
        preparedStatement.execute();
        preparedStatement1.execute();
        return true;

    }

    /**
     * @param login
     * @param pass
     * @return
     * @throws SQLException + Done
     */
    @Override
    public boolean changePassword(String login, String pass) throws SQLException {
        if (StringUtils.isEmpty(pass)) {
            throw new IllegalArgumentException("You can not use this form of password");
        }
        PreparedStatement ps = dataBase.getConnection().prepareStatement("update secret_data set password=? where login=?");
        ps.setInt(1, pass.hashCode());
        ps.setString(2, login);
        ps.execute();
        return true;
    }

    @Override
    public boolean changeFirstName(String login, String newFirstName) throws SQLException {

        if (StringUtils.isEmpty(login) || StringUtils.isEmpty(newFirstName)) {
            log.log(Level.SEVERE, "Wrong login form or you should consider your new name");
            throw new IllegalArgumentException("Wrong login form or you should consider your new name");
        }

        PreparedStatement ps = dataBase.getConnection().prepareStatement("update users set firstName=? where login=?");
        ps.setString(1, newFirstName);
        ps.setString(2, login);
        ps.execute();
        return true;
    }

    @Override
    public boolean changeSecondName(String login, String newSecondName) throws SQLException {
        if (StringUtils.isEmpty(login) || StringUtils.isEmpty(newSecondName)) {
            log.log(Level.SEVERE, "Wrong login form or you should consider your new name");
            throw new IllegalArgumentException("Wrong login form or you should consider your new second name");
        }

        PreparedStatement ps = dataBase.getConnection().prepareStatement("update users set secondName=? where login=?");
        ps.setString(1, newSecondName);
        ps.setString(2, login);
        ps.execute();
        return true;
    }

    @Override
    public boolean changeFatherName(String login, String fatherName) throws SQLException {
        if (StringUtils.isEmpty(login) || StringUtils.isEmpty(fatherName)) {
            log.log(Level.SEVERE, "Wrong login form or you should consider your new name");
            throw new IllegalArgumentException("Wrong login form or you should consider your new father name");
        }
        PreparedStatement ps = dataBase.getConnection().prepareStatement("update users set fatherName=? where login=?");
        ps.setString(1, fatherName);
        ps.setString(2, login);
        ps.execute();
        return true;
    }

    @Override
    public void setVacancyAndGoToInactive(String employee_login, String employer_login, String company, String vacancy) throws EmployeeDaoException {
//        Vacancy vacancy1;
//        Optional<Employer> optionalEmployer =
//                dataBase.getEmployers().stream().filter(p -> p.getLogin().equals(employerToken)).findFirst();
//        if (!optionalEmployer.isPresent()) {
//            throw new EmployeeDaoException("No Such User");
//        }
//        vacancy1 = optionalEmployer.get().getVacancies().get(vacancy);
//        Vacancy finalVacancy = vacancy1;
//        dataBase.getAllEmployees().stream().filter(p -> p.getLogin().equals(employeeToken)).findFirst().ifPresent(o -> {
//            dataBase.getActiveEmployees().remove(o);
//            o.setVacancy(finalVacancy);
//            o.setEmployerCompany(company);
//            o.setActive(false);
//            dataBase.getInactiveEmployees().add(o);
//        });


    }

    @Override
    public void addSkill(String login, Demand skill) throws EmployeeDaoException, SQLException {

        if (StringUtils.isEmpty(login) || skill == null) {
            throw new EmployeeDaoException("login or skill is wrong, check both of them");
        }
        String skillID = UUID.randomUUID().toString();
        PreparedStatement ps = dataBase.getConnection().prepareStatement("insert into skills(skill_id,name, level) VALUES (?,?,?)");
        PreparedStatement preparedStatement = dataBase.getConnection().prepareStatement("UPDATE employees SET skill_id=? WHERE login=?");

        preparedStatement.setString(1, skillID);
        preparedStatement.setString(2, login);

        ps.setString(1, skillID);
        ps.setString(2, skill.getDemandName());
        ps.setInt(3, skill.getLevel());

        ps.execute();
        preparedStatement.execute();

    }

    @Override
    public boolean deleteSkill(String token, String skillName) {

        return false;
    }

    @Override
    public boolean setNewLevelToSkill(String token, String skillName, int level) {

        return false;
    }

    /**
     * список всех вакансий работодателей, для которых его набор умений соответствует
     * требованиям работодателя на уровне не ниже уровня, указанного в требовании
     */
    @Override
    public Set<Vacancy> getVacanciesMostSuitable(String token) throws EmployeeDaoException {
//        Set<Vacancy> set = new HashSet<>();
//
//        Optional<Employee> optionalEmployee = dataBase.getAllEmployees().stream()
//                .filter(p -> p.getToken().equals(token)).findFirst();
//
//        if (!optionalEmployee.isPresent()) {
//            log.log(Level.SEVERE, "Employee does not exist");
//
//            throw new EmployeeDaoException("Employee does not exist");
//        }
//
//        Map<String, Demand> skills = optionalEmployee.get().getSkills();
//        for (Employer p : dataBase.getActiveEmployers())
//            for (Vacancy k : p.getVacancies().values()) {
//                int i = 0;
//                for (Demand d :
//                        k.getDemands().values())
//                    if (skills.containsKey(d.getDemandName())
//                            && skills.get(d.getDemandName()).getLevel() >= d.getLevel()) i++;
//                if (i == k.getDemands().size()) set.add(k);
//            }

//        return set;
        return null;
    }

    /**
     * список всех вакансий работодателей, для которых
     * его набор умений соответствует обязательным требованиям
     * работодателя на уровне не ниже уровня, указанного в требовании
     */
    @Override
    public Set<Vacancy> getNecessaryVacanciesNotLessThanEmployeeLevel(String token) throws EmployeeDaoException {
//        Set<Vacancy> set = new HashSet<>();
//
//        Optional<Employee> optionalEmployee = dataBase.getActiveEmployees().stream().filter(p -> p.getToken().equals(token)).findFirst();
//        if (!optionalEmployee.isPresent()) {
//            log.log(Level.SEVERE, "No such person is DataBase. ID ".concat(token));
//
//            throw new EmployeeDaoException(String.format("No such person is DataBase. ID= '%s'", token));
//        }
//
//        Map<String, Demand> skills = optionalEmployee.get().getSkills();
//        for (Employer p : dataBase.getActiveEmployers())
//            for (Vacancy k : p.getVacancies().values()) {
//                int i = 0;
//                Set<Demand> necessaries = k.getDemands().values().stream().filter(Demand::isNecessary).collect(Collectors.toSet());
//                for (Demand d :
//                        necessaries)
//                    if (skills.containsKey(d.getDemandName())
//                            && skills.get(d.getDemandName()).getLevel() >= d.getLevel()) i++;
//                if (i == necessaries.size()) set.add(k);
//            }
//
//        return set;
//
        return null;
    }

    /**
     * список всех вакансий работодателей, для которых его набор умений соответствует
     * требованиям работодателя на любом уровне
     */
    @Override
    public Set<Vacancy> getVacanciesAllLevel(String token) throws EmployeeDaoException {
//        Set<Vacancy> set = new HashSet<>();
//
//        Optional<Employee> optionalEmployee = dataBase.getActiveEmployees().stream().filter(p -> p.getToken().equals(token)).findFirst();
//        if (!optionalEmployee.isPresent()) {
//            log.log(Level.SEVERE, "No such person is DataBase or skills is not mentioned even one time");
//            throw new EmployeeDaoException("No such person is DataBase or skills is not mentioned even one time");
//        }
//
//        Map<String, Demand> skills = optionalEmployee.get().getSkills();
//        for (Employer p : dataBase.getActiveEmployers())
//            for (Vacancy k : p.getVacancies().values()) {
//                int i = 0;
//                for (Demand d :
//                        k.getDemands().values())
//                    if (skills.containsKey(d.getDemandName())) {
//                        i++;
//                        if (i == k.getDemands().size()) {
//                            set.add(k);
//                            break;
//                        }
//                    }
//
//            }
//
//        return set;
        return null;
    }

    /**
     * список всех вакансий работодателей, для которых работник имеет хотя бы одно умение из списка в требовании работодателя на уровне не ниже уровня,
     * указанного в требовании.
     * В этом случае список выдается отсортированным по числу найденных умений,
     * то есть в начале списка приводятся те вакансии работодателей, для которых работник имеет большее число умений.
     */
    @Override
    public Set<Vacancy> getVacanciesSuitableOneDemandNotLessThanLevel(String token) throws EmployeeDaoException {
//        Set<Vacancy> treeSet = new TreeSet<>((vacancy, t1) -> t1.getCount() - vacancy.getCount());
//
//        Optional<Employee> optionalEmployee = dataBase.getActiveEmployees().stream().filter(p -> p.getToken().equals(token)).findFirst();
//        if (!optionalEmployee.isPresent()) {
//            log.log(Level.SEVERE, "No such person is DataBase or skills is not mentioned even one time");
//            throw new EmployeeDaoException("No such person is DataBase or skills is not mentioned even one time");
//        }
//
//        Map<String, Demand> skills = optionalEmployee.get().getSkills();
//
//        for (Employer emp : dataBase.getActiveEmployers())
//            for (Vacancy vac : emp.getVacancies().values())
//                for (Demand demand : vac.getDemands().values()) {
//                    String d = demand.getDemandName();
//                    if (skills.containsKey(d) && skills.get(d).getLevel() >= demand.getLevel()) {
//                        vac.autoIncrement();
//                        treeSet.add(vac);
//                    }
//                }
//        return treeSet;
        return null;
    }
}
