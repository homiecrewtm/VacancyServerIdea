package hiring.clients.employee.daoemployee;

import hiring.clients.employee.Employee;
import hiring.clients.employee.daoemployee.daoexceptions.EmployeeDaoException;
import hiring.demand.Demand;
import hiring.vacancy.Vacancy;

import java.sql.SQLException;
import java.util.Set;

public interface DaoEmployee {
    Set<Vacancy> getVacanciesMostSuitable(String login) throws EmployeeDaoException;

    void setVacancyAndGoToInactive(String employerToken, String employeeToken, String company, String vacancy) throws EmployeeDaoException;

    Set<Vacancy> getNecessaryVacanciesNotLessThanEmployeeLevel(String login) throws EmployeeDaoException;

    Set<Vacancy> getVacanciesAllLevel(String login) throws EmployeeDaoException;

    Set<Vacancy> getVacanciesSuitableOneDemandNotLessThanLevel(String login) throws EmployeeDaoException;

    boolean insert(Employee employee) throws SQLException;

    boolean delete(String token) throws EmployeeDaoException, SQLException;

    boolean changePassword(String uuid, String pass) throws SQLException;

    boolean changeFirstName(String token, String newFirstName) throws SQLException;

    boolean changeSecondName(String token, String newSecondName) throws SQLException;

    boolean changeFatherName(String token, String fatherName) throws SQLException;

    boolean deleteSkill(String token, String skillName);

    void addSkill(String token, Demand skill) throws EmployeeDaoException, SQLException;

    boolean setNewLevelToSkill(String token, String skillName, int level);

}
