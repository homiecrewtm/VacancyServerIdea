package hiring.clients.employee;

import hiring.clients.User;
import hiring.demand.Demand;
import hiring.vacancy.Vacancy;

import java.util.*;

public class Employee extends User {
    private Vacancy vacancy;
    private String employerCompany;
    private String email;

    private Map<String, Demand> skills;

    public Employee(String firstName, String secondName,
                    String fatherName, String email, String login, String password) {
        super(firstName, secondName, fatherName, login, password);
        this.email = email;
        setSkills(new HashMap<>());

    }

    public Employee(String firstName, String secondName, String email,
                    String login, String password) {
        super(firstName, secondName, login, password);
        this.email = email;
        setSkills(new HashMap<>());

    }

    public void setVacancy(Vacancy vacancy) {
        this.vacancy = vacancy;
    }

    public Vacancy getVacancy() {
        return vacancy;
    }

    public String getEmployerCompany() {
        return employerCompany;
    }

    public void setEmployerCompany(String employerCompany) {
        this.employerCompany = employerCompany;
    }

    public boolean addSkill(Demand demand) {
        if (demand != null) {
            skills.put(demand.getDemandName(), demand);
            return true;
        }
        return false;
    }

    public String getEmail() {
        return email;
    }

    public Map<String, Demand> getSkills() {
        return skills;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Employee employee = (Employee) o;
        return Objects.equals(vacancy, employee.vacancy) &&
                Objects.equals(employerCompany, employee.employerCompany);
    }


    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), vacancy, employerCompany);
    }

    @Override
    public String toString() {
        return "token:" + super.getToken() +
                "; vacancy:" + vacancy +
                ", employerCompany:" + employerCompany +
                ", email:" + email +
                ", skills:" + skills.values() +
                '}';
    }

    public void setSkills(Map<String, Demand> skills) {
        this.skills = skills;
    }
}
