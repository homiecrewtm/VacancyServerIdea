package hiring.clients;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class User implements Serializable {
    private String secondName, firstName, fatherName, password;
    private final String login;
    private boolean isActive;
    private String token;

    public User(String firstName, String secondName, String fatherName, String login, String password) {
        setToken(UUID.randomUUID().toString());
        this.isActive = true;
        this.firstName = firstName.isEmpty() ? UUID.randomUUID().toString() : login;
        this.secondName = secondName;
        this.fatherName = fatherName;
        this.login = login;
        this.password = password.isEmpty() ? UUID.randomUUID().toString() : password;
    }

    public User(String firstName, String secondName, String login, String password) {
        setToken(UUID.randomUUID().toString());
        this.isActive = true;
        this.firstName = firstName;
        this.secondName = secondName;
        this.login = login;
        this.password = password;

    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isActive() {
        return isActive;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return isActive == user.isActive &&
                Objects.equals(secondName, user.secondName) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(fatherName, user.fatherName) &&
                Objects.equals(password, user.password) &&
                Objects.equals(login, user.login);
    }

    @Override
    public int hashCode() {

        return Objects.hash(secondName, firstName, fatherName, password, login, isActive);
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
