package hiring.clients.employer.daoexceptions;

public class EmployerDaoException extends Exception {
    public EmployerDaoException(String s) {
        super(s);
    }
}
