package hiring.clients.employer;

import hiring.clients.User;
import hiring.demand.Demand;
import hiring.vacancy.Vacancy;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class Employer extends User {
    private String companyName, address, email;
    private Map<String, Vacancy> vacancies;


    public Employer(String firstName, String secondName, String fatherName,
                    String login, String password, String companyName,
                    String address, String email) {
        super(firstName, secondName, fatherName, login, password);
        this.companyName = companyName;
        this.address = address;
        this.email = email;
        setVacancies(new HashMap<>());
    }


    public Employer(String firstName, String secondName, String login,
                    String password, String companyName, String address, String email) {
        super(firstName, secondName, login, password);
        this.companyName = companyName;
        this.address = address;
        this.email = email;
        setVacancies(new HashMap<>());
    }


    public boolean addVacancy(Vacancy vacancy) {
        if (vacancy != null) {
            vacancy.setCompanyName(companyName);
            vacancies.put(vacancy.getPosition(), vacancy);
            return true;
        }
        return false;

    }

    public boolean addDemandToVacancy(String vacancyName, Demand demand) {
        if (demand != null && !StringUtils.isEmpty(vacancyName)) {
            vacancies.get(vacancyName).addDemand(demand);
            return true;
        }
        return false;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    //Реализация добавления нескольких коллекций?

    public String getCompanyName() {
        return companyName;
    }

    public Map<String, Vacancy> getVacancies() {
        return vacancies;
    }

    public String getAddress() {
        return address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employer employer = (Employer) o;
        return Objects.equals(companyName, employer.companyName) &&
                Objects.equals(address, employer.address) &&
                Objects.equals(email, employer.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(companyName, address, email);
    }

    @Override
    public String toString() {
        return "Employer{ login=" +
                super.getLogin() + " companyName='" + companyName + '\'' +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", vacancies=" + vacancies +
                ", isActive=" + super.isActive() + "}";
    }

    public void setVacancies(Map<String, Vacancy> vacancies) {
        this.vacancies = vacancies;
    }
}
