package hiring.clients.employer.daoemployer;

import hiring.clients.employee.Employee;
import hiring.clients.employee.daoemployee.DAOEmployeeImpl;
import hiring.clients.employer.Employer;
import hiring.clients.employer.daoexceptions.EmployerDaoException;
import hiring.db.DataBase;
import hiring.db.dbexceptions.DbException;
import hiring.vacancy.Vacancy;
import org.apache.commons.lang3.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DAOEmployerImpl implements DaoEmployer {

    private DataBase dataBase;
    private static Logger log = Logger.getLogger(DAOEmployeeImpl.class.getName());


    public DAOEmployerImpl(DataBase dataBase) throws DbException {
        if (dataBase == null) throw new DbException();
        this.dataBase = dataBase;
    }

    /**
     * @param login
     * @return
     * @throws SQLException + Done
     */
    @Override
    public Map<String, Vacancy> getVacancies(String login) throws SQLException {
        HashMap<String, Vacancy> map = new HashMap<>();
        String employer_token = dataBase.getStatement()
                .executeQuery("select employer_token from employers where login='" + login + "';")
                .getString("employer_token");

        ResultSet resultSet = dataBase.getStatement()
                .executeQuery("select * from vacancies where employer_token='" + employer_token + "';");

        while (resultSet.next()) {
            Vacancy vacancy = new Vacancy(resultSet.getString("name"), resultSet.getString("companyName"),
                    resultSet.getFloat("salary"), resultSet.getBoolean("isActive"));
            vacancy.setToken(resultSet.getString("vacancy_id"));
            vacancy.setActive(resultSet.getBoolean("isActive"));
            vacancy.setSalary(resultSet.getFloat("salary"));
            map.put(vacancy.getPosition(), vacancy);
        }
        return map;
    }

    @Override
    public Set<Vacancy> getActiveVacancies(String login) throws SQLException {
        Set<Vacancy> map = new HashSet<>();
        String employer_token = dataBase.getStatement()
                .executeQuery("select employer_token from employers where login='" + login + "';")
                .getString("employer_token");
        ResultSet resultSet = dataBase.getStatement().executeQuery("select * from vacancies where employer_token='" + employer_token + "' and isActive=1;");
        while (resultSet.next()) {
            Vacancy vacancy = new Vacancy(resultSet.getString("name"),
                    resultSet.getString("companyName"),
                    resultSet.getFloat("salary"), resultSet.getBoolean("isActive"));
            vacancy.setToken(resultSet.getString("vacancy_id"));
            vacancy.setActive(resultSet.getBoolean("isActive"));
            map.add(vacancy);
        }
        return map;
    }

    /**
     * @param login
     * @return
     * @throws SQLException + Done
     */
    @Override
    public Set<Vacancy> getInactiveVacancies(String login) throws SQLException {
        Set<Vacancy> map = new HashSet<>();
        String employer_token = dataBase.getStatement()
                .executeQuery("select employer_token from employers where login='" + login + "';")
                .getString("employer_token");
        ResultSet resultSet = dataBase.getStatement().executeQuery("select * from vacancies where employer_token='" + employer_token + "' and isActive=0;");
        while (resultSet.next()) {
            Vacancy vacancy = new Vacancy(resultSet.getString("name"),
                    resultSet.getString("companyName"),
                    resultSet.getFloat("salary"), resultSet.getBoolean("isActive"));
            vacancy.setActive(resultSet.getBoolean("isActive"));
            vacancy.setToken(resultSet.getString("vacancy_id"));
            map.add(vacancy);
        }
        return map;    }


    /**
     * @param employer
     * @return + Done
     */
    @Override
    public boolean insert(Employer employer) {
        if (employer == null) {
            return false;
        }
        try {
            PreparedStatement preparedStatement1 = dataBase.getConnection().prepareStatement("insert into users(token, login, secondName, firstName, fatherName, email, isActive) values(?,?,?,?,?,?,?)");
            PreparedStatement preparedStatement3 = dataBase.getConnection().prepareStatement("insert into secret_data(login, password) values (?,?)");
            PreparedStatement preparedStatement4 = dataBase.getConnection().prepareStatement("insert into employers(employer_token,login, companyName, address) VALUES (?,?,?,?)");

            preparedStatement1.setString(1, employer.getToken());
            preparedStatement1.setString(2, employer.getLogin());
            preparedStatement1.setString(3, employer.getSecondName());
            preparedStatement1.setString(4, employer.getFirstName());
            preparedStatement1.setString(5, employer.getFatherName());
            preparedStatement1.setString(6, employer.getEmail());
            preparedStatement1.setBoolean(7, employer.isActive());


            preparedStatement3.setString(1, employer.getLogin());
            preparedStatement3.setInt(2, employer.getPassword().hashCode());

            preparedStatement4.setString(1, employer.getToken());
            preparedStatement4.setString(2, employer.getLogin());
            preparedStatement4.setString(3, employer.getCompanyName());
            preparedStatement4.setString(4, employer.getAddress());

            preparedStatement1.execute();
            preparedStatement3.execute();
            preparedStatement4.execute();
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.getMessage());
            return false;
        }
        return true;
    }


    /**
     * @param login
     * @return + Done
     */
    @Override
    public boolean deleteAccordingToLogin(String login) {
        try {
            PreparedStatement preparedStatement = dataBase.getConnection().prepareStatement("delete from employers where login=?");
            PreparedStatement preparedStatement1 = dataBase.getConnection().prepareStatement("delete from inactive_employers where login=?");
            PreparedStatement preparedStatement2 = dataBase.getConnection().prepareStatement("delete from secret_data where  login=?");
            preparedStatement2.setString(1, login);
            preparedStatement.setString(1, login);
            preparedStatement1.setString(1, login);
            preparedStatement.execute();
            preparedStatement1.execute();
            preparedStatement2.execute();

        } catch (SQLException ex) {
            log.log(Level.SEVERE, ex.getMessage());
            return false;
        }
        return true;


    }

    /**
     * @param login
     * @param newPassword
     * @throws SQLException + Done
     */
    @Override
    public void changePassword(String login, String newPassword) throws SQLException {
        if (StringUtils.isEmpty(login) || StringUtils.isEmpty(newPassword)) {
            throw new IllegalArgumentException("'" + login + "'  token is wrong or password is invalid");
        }

        try {
            ResultSet rs = dataBase.getStatement().executeQuery("select * from secret_data");

            String password = rs.getString(3);
            log.log(Level.WARNING, password);
            if (password.equals(String.valueOf(newPassword.hashCode()))) {
                throw new SQLException("You can not use the same password");
            }
            PreparedStatement changePasswd = dataBase.getConnection().prepareStatement("update secret_data set password=? where login=?");
            changePasswd.setLong(1, newPassword.hashCode());
            changePasswd.setString(2, login);
            changePasswd.execute();
        } catch (SQLException e) {
            log.log(Level.WARNING, e.getMessage());
        }

    }

    /**
     * @param login
     * @param newCompanyName
     * @throws SQLException + Done
     */
    @Override
    public void changeCompanyName(String login, String newCompanyName) throws SQLException {
        if (StringUtils.isEmpty(login)) {
            throw new IllegalArgumentException(login + " is Wrong login");
        }
        try {
            PreparedStatement ps = dataBase.getConnection().prepareStatement("update employers set companyName=? where login=?");
            ps.setString(1, newCompanyName);
            ps.setString(2, login);
            ps.execute();
        } catch (SQLException e) {
            log.log(Level.WARNING, e.getMessage());
        }


    }

    /**
     * @param login
     * @param newAddress
     * @throws SQLException + Done
     */
    @Override
    public void changeAddress(String login, String newAddress) throws SQLException {
        if (!StringUtils.isEmpty(login)) {
            PreparedStatement ps = dataBase.getConnection().prepareStatement("update employers set address=? where login=?");
            ps.setString(1, newAddress);
            ps.setString(2, login);
            ps.execute();
            return;
        }
        throw new IllegalArgumentException(login + " is wrong token");
    }

    /**
     * @param login
     * @param newEmail
     * @throws EmployerDaoException
     * @throws SQLException         + Done
     */
    @Override
    public void changeEmail(String login, String newEmail) throws EmployerDaoException, SQLException {
        if (StringUtils.isEmpty(login) || StringUtils.isEmpty(newEmail)) {
            throw new EmployerDaoException("Empty login or new Email");
        }

        PreparedStatement ps = dataBase.getConnection().prepareStatement("update users set email=? where login=?");
        ps.setString(1, newEmail);
        ps.setString(2, login);
        ps.execute();
    }

    /**
     * @param login
     * @param newFirstName
     * @throws EmployerDaoException
     * @throws SQLException         + Done
     */
    @Override
    public void setNewFirstName(String login, String newFirstName) throws EmployerDaoException, SQLException {
        if (StringUtils.isEmpty(login) || StringUtils.isEmpty(newFirstName)) {
            throw new EmployerDaoException("Empty login or new First name");
        }
        PreparedStatement ps = dataBase.getConnection().prepareStatement("update users set firstName =? where login=?;");
        ps.setString(1, newFirstName);
        ps.setString(2, login);
        ps.execute();
    }

    /**
     * @param login
     * @param newSecondName
     * @throws EmployerDaoException
     * @throws SQLException         + Done
     */
    @Override
    public void setNewSecondName(String login, String newSecondName) throws EmployerDaoException, SQLException {
        if (StringUtils.isEmpty(login) || StringUtils.isEmpty(newSecondName)) {
            throw new EmployerDaoException("Empty login or new Second name");
        }
        PreparedStatement ps = dataBase.getConnection().prepareStatement("update users set firstName =? where login=?;");
        ps.setString(1, newSecondName);
        ps.setString(2, login);
        ps.execute();
    }

    /**
     * @param login
     * @param newFatherName
     * @throws EmployerDaoException
     * @throws SQLException         + Done
     */
    @Override
    public void setNewFatherName(String login, String newFatherName) throws EmployerDaoException, SQLException {
        if (StringUtils.isEmpty(login) || StringUtils.isEmpty(newFatherName)) {
            throw new EmployerDaoException("Empty login or new Father name");
        }
        PreparedStatement ps = dataBase.getConnection().prepareStatement("update users set firstName =? where login=?;");
        ps.setString(1, newFatherName);
        ps.setString(2, login);
        ps.execute();
    }

    /**
     * @param login
     * @throws SQLException + Done
     */
    @Override
    public void setProfileActiveInactive(String login) throws SQLException {
        if (StringUtils.isEmpty(login)) {
            throw new IllegalArgumentException("Wrong login");
        }

        PreparedStatement setActive = dataBase.getConnection().prepareStatement("delete from inactive_employers where login=?");
        PreparedStatement setInactive = dataBase.getConnection().prepareStatement("insert into inactive_employers(login) values (?)");
        PreparedStatement updateActiveStatus = dataBase.getConnection().prepareStatement("update users set isActive=? where login=?;");
        PreparedStatement check = dataBase.getConnection().prepareStatement("select isActive from users where login=?");

        check.setString(1, login);
        setInactive.setString(1, login);
        setActive.setString(1, login);
        updateActiveStatus.setString(2, login);

        ResultSet rs = check.executeQuery();

        boolean loginForCheck = rs.getBoolean("isActive");

        if (loginForCheck) {
            updateActiveStatus.setBoolean(1, false);
            setInactive.execute();
            updateActiveStatus.execute();
            return;
        }
        updateActiveStatus.setBoolean(1, true);
        updateActiveStatus.execute();
        setActive.execute();

    }


    /**
     * список всех потенциальных работников,
     * имеющих все необходимые для этой вакансии умения на уровне не ниже уровня,
     * указанного в требовании
     */
    @Override
    public Set<Employee> getPotentialEmployeesHaveAllSkills(String token, String vacancy) throws EmployerDaoException {
//        Set<Employee> employees = new HashSet<>();
//        Optional<Vacancy> optionalVacancy = dataBase.getActiveVacancies().stream().filter(p -> p.getPosition().equals(vacancy)).findFirst();
//        if (!optionalVacancy.isPresent()) {
//
//            throw new EmployerDaoException("token does not exist or vacancy you have described does not exist in rows of database");
//        }
//        Map<String, Demand> demandMap = optionalVacancy.get().getDemands();
//
//        for (Employee emp : dataBase.getActiveEmployees()) {
//            int i = 0;
//            for (Demand skill : emp.getSkills().values()) {
//                String name = skill.getDemandName();
//                if (demandMap.containsKey(name)
//                        && demandMap.get(name).getLevel() <= skill.getLevel()) {
//                    i++;
//                    if (i == demandMap.size()) {
//                        employees.add(emp);
//                        break;
//                    }
//                }
//            }
//        }
//
//        return employees;
        return null;
    }


    /**
     * список всех потенциальных работников,
     * имеющих все обязательные требования на уровне не ниже уровня,
     * указанного в требовании
     */
    @Override
    public Set<Employee> getPotentialEmployeeSetWithNecessarySkillsNotLessThanLevel(String token, String vacancy) throws EmployerDaoException {
//        Set<Employee> employees = new HashSet<>();
//        Optional<Vacancy> optionalVacancy = dataBase.getActiveVacancies().stream().filter(p -> p.getPosition().equals(vacancy)).findFirst();
//        if (optionalVacancy.isPresent()) {
//
//            Map<String, Demand> demandMap = optionalVacancy.get().getDemands();
//            Set<Demand> necessaries = demandMap.values().stream().filter(Demand::isNecessary).collect(Collectors.toSet());
//
//            for (Employee employee : dataBase.getActiveEmployees()) {
//                int i = 0;
//                for (Demand skill : employee.getSkills().values()) {
//                    String name = skill.getDemandName();
//                    if (demandMap.containsKey(name)) {
//                        Demand demand = demandMap.get(name);
//                        if (necessaries.contains(demand) && skill.getLevel() >= demand.getLevel()) {
//                            i++;
//                        }
//                    }
//                    if (i == necessaries.size()) {
//                        employees.add(employee);
//                    }
//                }
//            }
//
//        } else throw new EmployerDaoException("Vacancy you described does not exist");
//
//        return employees;
        return null;
    }

    /**
     * список всех потенциальных работников,
     * имеющих все необходимые для этой вакансии умения на любом уровне
     */
    @Override
    public Set<Employee> getPotentialEmployeesHaveAllNeededForVacancy(String token, String vacancy) throws EmployerDaoException {
//        Set<Employee> employees = new HashSet<>();
//
//        Optional<Vacancy> optionalVacancy = dataBase.getActiveVacancies().stream().filter(p -> p.getPosition().equals(vacancy)).findFirst();
//        if (optionalVacancy.isPresent()) {
//            Map<String, Demand> demandMap = optionalVacancy.get().getDemands();
//
//            for (Employee employee : dataBase.getActiveEmployees()) {
//                int i = 0;
//                for (Demand skill : employee.getSkills().values()) {
//                    String name = skill.getDemandName();
//                    if (demandMap.containsKey(name)) {
//                        i++;
//                    }
//                }
//                if (i == demandMap.size()) {
//                    employees.add(employee);
//                }
//            }
//
//        } else throw new EmployerDaoException("Your vacancy does not exist");
//
//
//        return employees;
        return null;
    }

    /**
     * список всех потенциальных работников,
     * имеющих хотя бы одно  необходимое для этой вакансии умение на уровне не ниже уровня,
     * указанного в требовании
     */
    @Override
    public Set<Employee> getSetOfEmployeeHasOneNeededSkillNotLessThanLevel(String token, String vacancy) throws EmployerDaoException {
//        Set<Employee> employees = new HashSet<>();
//        Optional<Vacancy> optionalVacancy = dataBase.getActiveVacancies().stream().filter(p -> p.getPosition().equals(vacancy)).findFirst();
//        if (!optionalVacancy.isPresent()) {
//            throw new EmployerDaoException("Vacancy you described does not exist");
//        }
//        Map<String, Demand> demandMap = optionalVacancy.get().getDemands();
//
//        for (Employee employee : dataBase.getActiveEmployees())
//            for (Demand skill : employee.getSkills().values()) {
//                if (demandMap.containsKey(skill.getDemandName())) {
//                    Demand demand = demandMap.get(skill.getDemandName());
//                    if (skill.getLevel() >= demand.getLevel()) employees.add(employee);
//                }
//            }
//        return employees;
        return null;
    }
}
