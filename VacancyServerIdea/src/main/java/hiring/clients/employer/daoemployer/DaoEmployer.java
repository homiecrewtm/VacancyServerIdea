package hiring.clients.employer.daoemployer;

import hiring.clients.employee.Employee;
import hiring.clients.employee.daoemployee.daoexceptions.EmployeeDaoException;
import hiring.clients.employer.Employer;
import hiring.clients.employer.daoexceptions.EmployerDaoException;
import hiring.demand.Demand;
import hiring.vacancy.Vacancy;

import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

public interface DaoEmployer {

    Map<String, Vacancy> getVacancies(String login) throws SQLException;

    Set<Vacancy> getActiveVacancies(String token) throws SQLException;

    Set<Vacancy> getInactiveVacancies(String token) throws SQLException;

    boolean insert(Employer employer);

    boolean deleteAccordingToLogin(String login) throws SQLException;

    void changePassword(String token, String newPassword) throws SQLException;

    void changeCompanyName(String login, String newCompanyName) throws SQLException;

    void changeAddress(String token, String newAddress) throws SQLException;

    void changeEmail(String token, String newEmail) throws EmployerDaoException, SQLException;

    void setNewFirstName(String token, String newFirstName) throws EmployerDaoException, SQLException;

    void setNewSecondName(String token, String newSecondName) throws EmployerDaoException, SQLException;

    void setNewFatherName(String token, String newFatherName) throws EmployerDaoException, SQLException;

    void setProfileActiveInactive(String token) throws SQLException;

    Set<Employee> getPotentialEmployeesHaveAllSkills(String login, String vacancy) throws EmployerDaoException;

    Set<Employee> getPotentialEmployeeSetWithNecessarySkillsNotLessThanLevel(String login, String vacancy) throws EmployerDaoException;

    Set<Employee> getPotentialEmployeesHaveAllNeededForVacancy(String login, String vacancy) throws EmployerDaoException;

    Set<Employee> getSetOfEmployeeHasOneNeededSkillNotLessThanLevel(String login, String vacancy) throws EmployerDaoException;

}
