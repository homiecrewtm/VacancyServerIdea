package hiring.server;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import hiring.db.dbexceptions.DbException;
import hiring.dtoobjects.dtoclients.dtoemployee.DtoEmployee;
import hiring.clients.employee.daoemployee.DAOEmployeeImpl;
import hiring.clients.employee.daoemployee.daoexceptions.EmployeeDaoException;
import hiring.clients.employer.Employer;
import hiring.clients.employer.daoemployer.DAOEmployerImpl;
import hiring.clients.employer.daoexceptions.EmployerDaoException;
import hiring.db.DataBase;
import hiring.demand.daodemand.DaoDemandImpl;
import hiring.dtoobjects.dtoclients.dtoemployee.dtoemployeechangefirstname.DtoEmployeeChangeFirstName;
import hiring.dtoobjects.dtoclients.dtoemployee.dtoemployeetodelete.DtoEmployeeToDelete;
import hiring.dtoobjects.dtoclients.dtoemployertodelete.DtoDeleteEmployer;
import hiring.dtoobjects.dtoclients.dtoemployertodelete.DtoEmployerToDeleteByToken;
import hiring.dtoobjects.dtoclients.dtoemployertodelete.DtoToDeleteEmployerByLogin;
import hiring.dtorequests.*;
import hiring.dtorequests.employeeresponse.*;
import hiring.dtorequests.employerresponce.*;
import hiring.dtorequests.dtovacancy.*;
import hiring.errors.errorofemployer.EmployerError;
import hiring.jsonresponces.employerresponces.EmployerResp;
import hiring.service.jsonservice.CheckJsonService;
import hiring.service.userservice.employeeservice.EmployeeService;
import hiring.service.userservice.employerservice.EmployerService;
import hiring.vacancy.daovacancy.DaoVacancyImpl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.SQLException;

import static hiring.service.jsonservice.CheckJsonService.isJsonValid;

public class Server implements Serializable {

    private static DataBase dataBase;
    private EmployeeService employeeService;
    private EmployerService employerService;
    private Gson gson = new Gson();
    private static Logger log = LoggerFactory.getLogger(Server.class.getName());
    private static Server server;

    static {
        try {
            server = new Server();
        } catch (DbException | SQLException e) {
            log.warn(e.getMessage());
        }
    }

    public static DataBase getDataBase() {
        return dataBase;
    }

    public DataBase getDB() {
        return dataBase;
    }

    private Server() throws DbException, SQLException, NullPointerException {
        DAOEmployerImpl daoEmployer;
        DAOEmployeeImpl daoEmployee;
        DaoVacancyImpl daoVacancy;
        DaoDemandImpl daoDemand;

        dataBase = new DataBase();

        daoEmployer = new DAOEmployerImpl(dataBase);
        daoVacancy = new DaoVacancyImpl(dataBase, daoEmployer);
        daoEmployee = new DAOEmployeeImpl(dataBase);
        daoDemand = new DaoDemandImpl(dataBase);
        employerService = new EmployerService(daoEmployer, daoVacancy, daoDemand);
        employeeService = new EmployeeService(daoEmployee, daoVacancy);

    }

    public static Server startServer() {
        return server;
    }

    public void stopServer() throws IOException {
//        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File("DataBase.txt")))) {
//            bw.write(new Gson().toJson(dataBase));
//        } catch (FileNotFoundException e) {
//            System.out.println("There are no Data Base");
//        } finally {
//            dataBase = null;
//        }
    }


    public void clear() throws DbException, SQLException {
        dataBase.getStatement().execute("delete from inactive_employees");
        dataBase.getStatement().execute("delete from inactive_employers");
        dataBase.getStatement().execute("delete from demands");
        dataBase.getStatement().execute("delete from employees");
        dataBase.getStatement().execute("delete from employers");
        dataBase.getStatement().execute("delete from secret_data");
        dataBase.getStatement().execute("delete from users");
        dataBase.getStatement().execute("delete from vacancies");
        dataBase.getStatement().execute("delete from vacancies_employers");
        dataBase.getStatement().execute("delete from skills");

    }

    public void drop() throws SQLException {
        dataBase.getStatement().execute("drop table employees");
        dataBase.getStatement().execute("drop table inactive_employees");
        dataBase.getStatement().execute("drop table inactive_employers");
        dataBase.getStatement().execute("drop table demands");
        dataBase.getStatement().execute("drop table employers");
        dataBase.getStatement().execute("drop table secret_data");
        dataBase.getStatement().execute("drop table users");
        dataBase.getStatement().execute("drop table vacancies");
        dataBase.getStatement().execute("drop table vacancies_employers");
        dataBase.getStatement().execute("drop table skills");
    }


    public String registerEmployee(String jsonEmployee) throws SQLException {

        if (CheckJsonService.checkIfEmployee(jsonEmployee)) {
            return employeeService.registerEmployee(jsonEmployee);
        }
        return gson.toJson(new EmployerError("invalid json string"));

    }

    public String deleteEmployee(String jsonToken) throws EmployeeDaoException {

//        DtoEmployee dtoEmployee = gson.fromJson(jsonToken, DtoEmployee.class);
//        if (StringUtils.isEmpty(dtoEmployee.getToken())) {
//            return gson.toJson(new EmployerError("You put the wrong login"));
//        }
//        return employeeService.deleteEmployee(dtoEmployee.getToken());
        return null;
    }

    public String changeUserPassword(String jsonEmployee) throws SQLException {

        DtoEmployeeToDelete dtoEmployee = gson.fromJson(jsonEmployee, DtoEmployeeToDelete.class);
        if (!StringUtils.isEmpty(dtoEmployee.getLogin()) && !StringUtils.isEmpty(dtoEmployee.getPassword())) {
            return employeeService.changePassword(dtoEmployee.getLogin(), dtoEmployee.getPassword());
        }
        return gson.toJson(new EmployerError("You put the wrong login"));

    }

    public String changeUserFirstName(String jsonLoginPlusNewName) throws SQLException {
        DtoEmployeeChangeFirstName dtoEmployee = gson.fromJson(jsonLoginPlusNewName, DtoEmployeeChangeFirstName.class);
        if (StringUtils.isEmpty(dtoEmployee.getLogin()) && StringUtils.isEmpty(dtoEmployee.getFirstName())) {
            return gson.toJson(new EmployerError("Please check new name you want to set or there problems with json"));
        }
        return employeeService.changeEmployeeFirstName(dtoEmployee.getLogin(), dtoEmployee.getFirstName());
    }

    public String registerEmployer(String jsonEmployer) {
        if (CheckJsonService.checkJsonIfEmployer(jsonEmployer)) {
            Employer employer = gson.fromJson(jsonEmployer, Employer.class);
            return employerService.registerEmployer(employer);
        }
        return gson.toJson(new EmployerError("Bad employer data, badly built json"));
    }

    public String changeUserSecondName(String jsonTokenAndNewName) {
        DtoEmployee dtoEmployee = gson.fromJson(jsonTokenAndNewName, DtoEmployee.class);
        if (StringUtils.isEmpty(dtoEmployee.getToken()) && StringUtils.isEmpty(dtoEmployee.getSecondName())) {

            return gson.toJson(new EmployerError("Bad token or second name"));
        }
//        return employeeService.changeEmployeeSecondName(dtoEmployee);
        return null;
    }

    public String changeUserFatherName(String jsonTokenAndFatherName) {
        DtoEmployee dtoEmployee = gson.fromJson(jsonTokenAndFatherName, DtoEmployee.class);
        if (StringUtils.isEmpty(dtoEmployee.getToken())) {
            return gson.toJson(new EmployerError("Bad token"));
        }
//        return employeeService.changeFatherName(dtoEmployee);
        return null;

    }

    public String setEmployeeNewLevelOfSkill(String jsonTokenDemNameLevel) {
        DtoNewSkillLevelRequest dto = gson
                .fromJson(jsonTokenDemNameLevel, DtoNewSkillLevelRequest.class);
        if (StringUtils.isEmpty(dto.getLogin())) {
            return gson.toJson(new EmployerError("Bad token"));
        }
//        return employeeService.setEmployeeNewLevelOfSkill(dto);
        return null;

    }

    public String setVacancyAndGoToInactiveEmployees(String jsonTokenCompanyVacancy) throws EmployeeDaoException {
        DtoSetVacancyToEmployee dto = gson.fromJson(jsonTokenCompanyVacancy, DtoSetVacancyToEmployee.class);
//        return employeeService.setVacancyAndGoToInactiveEmployees(dto);
        return null;
    }


    public String getEmployerVacancies(String jsonToken) throws SQLException {
        DtoVacanciesRequestFOrEmployee dto = gson
                .fromJson(jsonToken, DtoVacanciesRequestFOrEmployee.class);
        return employerService.getVacancies(dto);
    }

    public String getEmployerActiveVacancies(String jsonToken) throws SQLException {
        DtoVacanciesRequestFOrEmployee dto = gson
                .fromJson(jsonToken, DtoVacanciesRequestFOrEmployee.class);
        return employerService.getActiveVacancies(dto);
    }

    public String getEmployerInactiveVacancies(String jsonToken) throws SQLException {
        DtoVacanciesRequestFOrEmployee dto = gson
                .fromJson(jsonToken, DtoVacanciesRequestFOrEmployee.class);
        return employerService.getInactiveVacancies(dto);
    }

    public String deleteEmployerAccordingToLogin(String jsonLogin) throws SQLException {
        DtoToDeleteEmployerByLogin dto = gson.fromJson(jsonLogin, DtoToDeleteEmployerByLogin.class);
        return employerService.deleteEmployerAccordingToLogin(dto);

    }


    public String addVacancyToEmployer(String jsonTokenAndVacancy) throws EmployerDaoException, SQLException {
        DtoRequestToAddVacancytoEmployer dto = gson.fromJson(jsonTokenAndVacancy, DtoRequestToAddVacancytoEmployer.class);
        if (employerService.addVacancy(dto)) {
            return gson.toJson(new EmployerResp(dto.getLogin(), dto.getPosition()));
        }
        return gson.toJson(new EmployerError("Bad vacancy description or ID invalid"));
    }

    public String setVacancyActiveInactive(String jsonTokenVacancy) {
        DtoSetVacancyActiveInactiveRequest dto = gson.fromJson(jsonTokenVacancy,
                DtoSetVacancyActiveInactiveRequest.class);
        return employerService.setVacancyActiveInactive(dto);
    }

    public String addSkillToEmployee(String jsonLoginSkill) throws EmployeeDaoException, SQLException {
        DtoAddSkillToEmployeeRequest dto = gson.fromJson(jsonLoginSkill, DtoAddSkillToEmployeeRequest.class);
        return employeeService.addSkill(dto);
    }

    public String deleteEmployeeSkill(String jsonTokenSkillName) {
//        DtoDeleteEmployeeSkillRequest dto = gson.fromJson(jsonTokenSkillName,
//                DtoDeleteEmployeeSkillRequest.class);
//        return employeeService.deleteSkill(dto);
        return null;
    }

    public String addDemandToVacancyOfEmployer(String jsonTokenVacancyDemand) throws EmployerDaoException, SQLException {

        DtoAddDemandToVacancyReq dto = gson.fromJson(jsonTokenVacancyDemand, DtoAddDemandToVacancyReq.class);
        return employerService.addDemandToVacancy(dto);
    }

    public String changeEmployerVacancyDemandName(String jsonTokenVacancyDemandNewDemandName) {
        DtoChangeVacancyDemandName dto = gson.fromJson(jsonTokenVacancyDemandNewDemandName, DtoChangeVacancyDemandName.class);
        return employerService.changeVacancyDemandName(dto);
    }

    public String changeDemandLevel(String jsonTokenVacancyDemandNameLeve) {
        DtoReqToSetNEwLevelToDemand dto = gson.fromJson(jsonTokenVacancyDemandNameLeve, DtoReqToSetNEwLevelToDemand.class);
        return employerService.changeDemandLevel(dto);
    }

    public String changeEmployerVacancyDemandIsNecessary(String jsonTokenVacancyDemand) {
        DtoReqToChangeNecessaryOofDemand dto = gson.fromJson(jsonTokenVacancyDemand, DtoReqToChangeNecessaryOofDemand.class);
        return employerService.changeDemandIsNecessary(dto);
    }

    public String removeVacancyFromEmployer(String jsonTokenVacancy) throws SQLException {
        DtoForRemoveVacancyFromEmployer dto = gson.fromJson(jsonTokenVacancy, DtoForRemoveVacancyFromEmployer.class);
        return employerService.deleteVacancy(dto);
    }

    public String changeEmployerCompanyName(String jsonLoginCompanyName) {
        DtoChangeEmployerCompanyName dto = gson.fromJson(jsonLoginCompanyName, DtoChangeEmployerCompanyName.class);
        return employerService.changeCompanyName(dto);
    }

    public String changeEmployerAddress(String jsonLoginAddress) throws SQLException {
        DtoChangeEmployerAddressReq dto = gson.fromJson(jsonLoginAddress, DtoChangeEmployerAddressReq.class);
        return employerService.changeAddress(dto);
    }

    public String changeEmployerPassword(String jsonLoginNewPassword) {
        DtoChangeEmployerPassReq dto = gson.fromJson(jsonLoginNewPassword, DtoChangeEmployerPassReq.class);
        return employerService.changePassword(dto);
    }

    public String changeEmployerFirstName(String jsonTokenFirstName) throws EmployerDaoException, SQLException {
        DtoChangeEmployerFirstName dto = gson.fromJson(jsonTokenFirstName, DtoChangeEmployerFirstName.class);
        return employerService.changeFirstName(dto);

    }

    public String changeEmployerSecondName(String jsonTokenSecondName) throws EmployerDaoException, SQLException {
        DtoChangeEmployerSecondName dto = gson.fromJson(jsonTokenSecondName, DtoChangeEmployerSecondName.class);
        return employerService.changeSecondName(dto);
    }

    public String changeFatherName(String jsonTokenFatherName) throws EmployerDaoException, SQLException {
        DtoChangeEmployerFatherName dto = gson.fromJson(jsonTokenFatherName, DtoChangeEmployerFatherName.class);
        return employerService.changeFatherName(dto);
    }

    public String changeEmployerEmail(String jsonLoginEmail) {
        DtoChangeEmployerEmaiReq dto = gson.fromJson(jsonLoginEmail, DtoChangeEmployerEmaiReq.class);
        return employerService.changeEmail(dto);
    }

    public String setProfileOfEmployerActiveInactive(String jsonToken) {
        DtoSetProfActiveInactive dto = gson.fromJson(jsonToken, DtoSetProfActiveInactive.class);
        return employerService.setProfileActiveInactive(dto);
    }

    //Employer service
    public String getPotentialEmployeesHaveAllSkills(String jsonTokenVacancy) {
        DtoForUsageOnleVacancy dto = gson.fromJson(jsonTokenVacancy, DtoForUsageOnleVacancy.class);
        return employerService.getPotentialEmployeesHaveAllSkills(dto);
    }

    public String getPotentialEmployeeSetWithNecessarySkillsNotLessThanLevel(String jsonTokenVacancy) {
        DtoForUsageOnleVacancy dto = gson.fromJson(jsonTokenVacancy, DtoForUsageOnleVacancy.class);
        return employerService.getPotentialEmployeeSetWithNecessarySkillsNotLessThanLevel(dto);
    }

    public String getPotentialEmployeesHaveAllNeededForVacancy(String jsonTokenVacancy) {
        DtoForUsageOnleVacancy dto = gson.fromJson(jsonTokenVacancy, DtoForUsageOnleVacancy.class);
        return employerService.getPotentialEmployeesHaveAllNeededForVacancy(dto);
    }

    public String getSetOfEmployeeHasOneNeededSkillNotLessThanLevel(String jsonTokenVacancy) {
        DtoForUsageOnleVacancy dto = gson.fromJson(jsonTokenVacancy, DtoForUsageOnleVacancy.class);

        return employerService.getSetOfEmployeeHasOneNeededSkillNotLessThanLevel(dto);
    }
    //Employer service


    //Employee service
    public String getVacanciesMostSuitable(String jsonToken) {
        DtoParent dto = gson.fromJson(jsonToken, DtoParent.class);
//        return employeeService.getVacanciesMostSuitableForEmployee(dto);
        return null;
    }


    public String getNecessaryVacanciesNotLessThanEmployeeLevel(String jsonToken) {
        DtoParent dto = gson.fromJson(jsonToken, DtoParent.class);
        return null;
//        return employeeService.getNecessaryVacanciesNotLessThanEmployeeLevel(dto);
    }

    public String getVacanciesAllLevel(String jsonToken) {
        DtoParent dto = gson.fromJson(jsonToken, DtoParent.class);
        return null;
//        return employeeService.getVacanciesAllLevel(dto);
    }

    public String getVacanciesSuitableOneDemandNotLessThanLevel(String jsonToken) {
        DtoParent dto = gson.fromJson(jsonToken, DtoParent.class);
        return null;
//        return employeeService.getVacanciesSuitableOneDemandNotLessThanLevel(dto);
    }
    //Employee service


}
