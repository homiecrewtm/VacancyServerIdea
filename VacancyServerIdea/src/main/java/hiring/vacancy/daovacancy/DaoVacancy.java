package hiring.vacancy.daovacancy;

import hiring.clients.employer.daoexceptions.EmployerDaoException;
import hiring.vacancy.Vacancy;

import java.sql.SQLException;

public interface DaoVacancy {
    boolean removeVacancy(String vacancy_position) throws SQLException;

    boolean addVacancy(String token, Vacancy vacancy) throws EmployerDaoException, SQLException;

    void setVacancyActiveInactive(String login,String vacancy) throws SQLException;

}
