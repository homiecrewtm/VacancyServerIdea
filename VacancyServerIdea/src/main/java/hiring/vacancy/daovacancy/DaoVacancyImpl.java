package hiring.vacancy.daovacancy;

import hiring.clients.employer.daoemployer.DaoEmployer;
import hiring.db.DataBase;
import hiring.vacancy.Vacancy;
import org.apache.commons.lang3.StringUtils;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

public class DaoVacancyImpl implements DaoVacancy {

    private DataBase dataBase;
    private DaoEmployer daoEmployer;

    public DaoVacancyImpl(DataBase dataBase, DaoEmployer daoEmployer) {
        this.dataBase = dataBase;
        this.daoEmployer = daoEmployer;
    }

    @Override
    public boolean removeVacancy(String vacancy_position) throws SQLException {
        if (StringUtils.isEmpty(vacancy_position)) {
            throw new IllegalArgumentException("Wring login or check vacancy you described");
        }
        PreparedStatement preparedStatement1 = dataBase.getConnection().prepareStatement("DELETE FROM vacancies WHERE name=?");
        preparedStatement1.setString(1, vacancy_position);
        preparedStatement1.execute();
        return true;
    }

    @Override
    public boolean addVacancy(String login, Vacancy vacancy) throws SQLException {
        if (vacancy == null || StringUtils.isEmpty(login)) {
            throw new IllegalArgumentException("Wring login or check vacancy you described");
        }
        vacancy.setToken(UUID.randomUUID().toString());
        PreparedStatement ps = dataBase.getConnection().prepareStatement("INSERT INTO vacancies(vacancy_id, name," +
                " isActive, companyName, salary, employer_token) VALUES (?,?,?,?,?,?);");

        ps.setString(1, vacancy.getToken());
        ps.setString(2, vacancy.getPosition());
        ps.setBoolean(3, vacancy.isActive());
        ps.setString(4, vacancy.getCompanyName());
        ps.setFloat(5, vacancy.getSalary());
        ps.setString(6, dataBase.getStatement().executeQuery("select employer_token  from employers where login='" + login + "';")
                .getString("employer_token"));

        ps.execute();

        return true;
    }

    @Override
    public void setVacancyActiveInactive(String login, String vacancy) throws SQLException {
        if (StringUtils.isEmpty(vacancy)) {
            throw new NullPointerException("Please check your data about vacancy");
        }
        String employer_id = dataBase.getStatement().executeQuery("select employer_token from employers where login='" + login + "';")
                .getString("employer_token");

        boolean b = dataBase.getStatement().executeQuery("select isActive from vacancies where employer_token='" + employer_id + "' and name='" + vacancy + "';")
                .getBoolean("isActive");

        PreparedStatement preparedStatement = dataBase.getConnection().prepareStatement("update vacancies set isActive=? where name=?;");

        preparedStatement.setInt(1, b ? 0 : 1);
        preparedStatement.setString(2, vacancy);

        preparedStatement.execute();


    }

}
