package hiring.vacancy;

import hiring.demand.Demand;

import java.io.Serializable;
import java.util.*;

public class Vacancy implements Serializable {

    private boolean isActive;
    private String companyName;
    private String position; // должность
    private float salary;//предполагаемый оклад
    private int count;
    private String token;
    private Map<String, Demand> demands;

    public Vacancy(String position, float salary, String company) {
        this.isActive = true;
        this.position = position;
        this.salary = salary;
        this.companyName = company;
        setDemands(new HashMap<>());
    }

    public Vacancy(String position, String company, float salary) {
        this.isActive = true;
        this.position = position;
        this.companyName = company;
        this.salary = salary;
        setDemands(new HashMap<>());
    }

    public Vacancy(String position, String company, float salary, boolean isActive) {
        this.isActive = isActive;
        this.position = position;
        this.salary = salary;
        this.companyName = company;
        setDemands(new HashMap<>());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getCount() {
        return count;
    }

    public void autoIncrement() {
        count++;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isActive() {
        return isActive;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public Map<String, Demand> getDemands() {
        return demands;
    }

    public void setDemandName(String oldName, String newName) {
        Demand demand = demands.get(oldName);
        demands.remove(oldName);
        demand.setDemandName(newName);
        demands.put(demand.getDemandName(), demand);
    }

    public void setSkillLevel(String demandName, int newLevel) {
        demands.get(demandName).setLevel(newLevel);
    }

    public boolean addDemand(Demand demand) {
        if (demand != null) {
            demands.put(demand.getDemandName(), demand);
            return true;
        }
        return false;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vacancy vacancy1 = (Vacancy) o;
        return isActive == vacancy1.isActive &&
                salary == vacancy1.salary &&
                Objects.equals(companyName, vacancy1.companyName) &&
                Objects.equals(position, vacancy1.position) &&
                Objects.equals(demands, vacancy1.demands);
    }

    @Override
    public int hashCode() {

        return Objects.hash(isActive, companyName, position, salary, demands);
    }

    @Override
    public String toString() {
        return "Vacancy{" +
                "isActive:" + isActive +
                ", companyName:" + companyName +
                ", position:" + position +
                ", salary:" + salary +
                ", demands:" + demands +
                '}';
    }

    public void setDemands(Map<String, Demand> demands) {
        this.demands = demands;
    }
}
