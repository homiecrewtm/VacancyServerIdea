package hiring.dtoobjects.dtoclients.dtouser;

public class DtoUser {

    private String secondName, firstName, fatherName, password;
    private String login;
    private boolean isActive;
    private String token;

    public DtoUser(String firstName, String secondName, String fatherName, String login, String password) {
        this.isActive = true;
        this.firstName = firstName;
        this.secondName = secondName;
        this.fatherName = fatherName;
        this.login = login;
        this.password = password;
    }

    public DtoUser(String firstName, String secondName, String login, String password) {
        this.isActive = true;
        this.firstName = firstName;
        this.secondName = secondName;
        this.login = login;
        this.password = password;
    }

    public DtoUser(String token) {
        this.token = token;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
