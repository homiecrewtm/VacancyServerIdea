package hiring.dtoobjects.dtoclients.dtoemployer;

import hiring.dtoobjects.dtoclients.dtouser.DtoUser;
import hiring.vacancy.Vacancy;

import java.util.HashMap;
import java.util.Map;

public class DtoEmployer extends DtoUser {

    private String companyName, address, email;
    private Map<String, Vacancy> vacancies;

    public DtoEmployer(String firstName, String secondName, String fatherName, String login, String password) {
        super(firstName, secondName, fatherName, login, password);
    }

    public DtoEmployer(String firstName, String secondName, String login, String password) {
        super(firstName, secondName, login, password);
    }

    public DtoEmployer(String firstName, String secondName, String fatherName,
                    String login, String password, String companyName,
                    String address, String email) {
        super(firstName, secondName, fatherName, login, password);
        this.companyName = companyName;
        this.address = address;
        this.email = email;
        setVacancies(new HashMap<>());
    }

    public DtoEmployer(String firstName, String secondName, String login,
                    String password, String companyName, String address, String email) {
        super(firstName, secondName, login, password);
        this.companyName = companyName;
        this.address = address;
        this.email = email;
    }

    public DtoEmployer(String token) {
        super(token);
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, Vacancy> getVacancies() {
        return vacancies;
    }

    private void setVacancies(Map<String, Vacancy> vacancies) {
        this.vacancies = vacancies;
    }
}
