package hiring.dtoobjects.dtoclients.dtoemployee.dtoemployeetodelete;

import hiring.dtorequests.DtoParent;

public class DtoEmployeeToDelete extends DtoParent {

   private String password;

    public DtoEmployeeToDelete(String login, String password) {
        super(login);
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
