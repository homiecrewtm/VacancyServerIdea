package hiring.dtoobjects.dtoclients.dtoemployee.dtoemployeechangesecondname;

import hiring.dtoobjects.dtoclients.dtoemployee.DtoChangeEmployee;

public class DtoEmployeeChangeSecondName extends DtoChangeEmployee {

    private String secondName;

    public DtoEmployeeChangeSecondName(String login, String secondName) {
        super(login);
        this.secondName = secondName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
}
