package hiring.dtoobjects.dtoclients.dtoemployee;

import hiring.dtoobjects.dtoclients.dtouser.DtoUser;
import hiring.demand.Demand;
import hiring.vacancy.Vacancy;

import java.util.Map;

public class DtoEmployee extends DtoUser {
    private Vacancy vacancy;
    private String employerCompany;
    private String email;

    private Map<String, Demand> skills;

    public DtoEmployee(String firstName, String secondName,
                    String fatherName, String email, String login, String password) {
        super(firstName, secondName, fatherName, login, password);
        this.email = email;

    }

    public DtoEmployee(String firstName, String secondName, String email,
                    String login, String password) {
        super(firstName, secondName, login, password);
        this.email = email;
    }

    public void setVacancy(Vacancy vacancy) {
        this.vacancy = vacancy;
    }

    public Vacancy getVacancy() {
        return vacancy;
    }

    public String getEmployerCompany() {
        return employerCompany;
    }

    public void setEmployerCompany(String employerCompany) {
        this.employerCompany = employerCompany;
    }

    public boolean addSkill(Demand demand) {
        if (demand != null) {
            skills.put(demand.getDemandName(), demand);
            return true;
        }
        return false;
    }

    public String getEmail() {
        return email;
    }

    public Map<String, Demand> getSkills() {
        return skills;
    }


    public void setSkills(Map<String, Demand> skills) {
        this.skills = skills;
    }


}
