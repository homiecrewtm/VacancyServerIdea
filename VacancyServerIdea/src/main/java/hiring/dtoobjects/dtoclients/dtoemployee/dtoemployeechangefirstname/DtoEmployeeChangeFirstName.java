package hiring.dtoobjects.dtoclients.dtoemployee.dtoemployeechangefirstname;

import hiring.dtoobjects.dtoclients.dtoemployee.DtoChangeEmployee;

public class DtoEmployeeChangeFirstName extends DtoChangeEmployee {
    private String firstName;

    public DtoEmployeeChangeFirstName(String login, String firstName) {
        super(login);

        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
