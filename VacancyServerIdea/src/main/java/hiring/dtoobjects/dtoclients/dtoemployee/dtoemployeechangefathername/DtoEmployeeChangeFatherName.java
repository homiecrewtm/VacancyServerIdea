package hiring.dtoobjects.dtoclients.dtoemployee.dtoemployeechangefathername;

import hiring.dtoobjects.dtoclients.dtoemployee.DtoChangeEmployee;

public class DtoEmployeeChangeFatherName extends DtoChangeEmployee {

    private String fatherName;

    public DtoEmployeeChangeFatherName(String login, String fatherName) {
        super(login);
        this.fatherName = fatherName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }
}
