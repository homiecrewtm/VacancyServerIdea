package hiring.dtoobjects.dtoclients.dtoemployee;

public class DtoChangeEmployee {

    private String login;

    public DtoChangeEmployee(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

}
