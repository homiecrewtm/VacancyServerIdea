package hiring.dtoobjects.dtoclients.dtoemployertodelete;

public class DtoToDeleteEmployerByLogin extends DtoDeleteEmployer {
    String login;

    public DtoToDeleteEmployerByLogin(String login) {
        this.login = login;
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
