package hiring.dtoobjects.dtoclients.dtoemployertodelete;

public class DtoEmployerToDeleteByToken extends DtoDeleteEmployer {

    private String token;

    public DtoEmployerToDeleteByToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
