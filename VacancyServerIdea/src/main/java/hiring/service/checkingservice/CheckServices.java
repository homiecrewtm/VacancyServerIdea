package hiring.service.checkingservice;

import java.util.Map;
import java.util.Set;

public class CheckServices {

    public static boolean checkString(String check) {
        return check != null && !check.equals("");
    }

    public static boolean checkMap(Map<?, ?> map) {
        return map != null;
    }

    public static boolean checkSet(Set<?> set) {
        return set != null && set.size() > 0;
    }

}
