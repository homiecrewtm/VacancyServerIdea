package hiring.service.userservice.employerservice;

import com.google.gson.Gson;
import hiring.clients.employee.Employee;
import hiring.clients.employee.daoemployee.daoexceptions.EmployeeDaoException;
import hiring.clients.employer.Employer;
import hiring.clients.employer.daoemployer.DaoEmployer;
import hiring.clients.employer.daoexceptions.EmployerDaoException;
import hiring.demand.Demand;
import hiring.demand.daodemand.DaoDemand;
import hiring.dtoobjects.dtoclients.dtoemployertodelete.DtoToDeleteEmployerByLogin;
import hiring.dtorequests.*;
import hiring.dtorequests.employeeresponse.DtoVacanciesRequestFOrEmployee;
import hiring.dtorequests.employerresponce.*;
import hiring.dtorequests.dtovacancy.*;
import hiring.errors.errorofemployer.EmployerError;
import hiring.jsonresponces.employerresponces.EmployerResp;
import hiring.vacancy.Vacancy;
import hiring.vacancy.daovacancy.DaoVacancy;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EmployerService {
    private DaoEmployer daoEmployer;
    private DaoVacancy daoVacancy;
    private DaoDemand daoDemand;
    private static Gson gson = new Gson();

    private Logger log = Logger.getLogger(EmployerService.class.getName());


    public EmployerService(DaoEmployer daoEmployer, DaoVacancy daoVacancy, DaoDemand daoDemand) {
        this.daoEmployer = daoEmployer;
        this.daoVacancy = daoVacancy;
        this.daoDemand = daoDemand;
    }

    public String registerEmployer(Employer employer) {
        employer.setActive(true);
        employer.setVacancies(new HashMap<>());
        employer.setToken(UUID.randomUUID().toString());
        if (!daoEmployer.insert(employer)) {
            return gson.toJson(new EmployerError("There are one employee with same login."));
        }
        return gson.toJson(new EmployerResp(employer.getLogin(), "OK"));

    }

    public String deleteEmployerAccordingToLogin(DtoToDeleteEmployerByLogin dto) throws SQLException {
        String dtoLogin = dto.getLogin();
        if (StringUtils.isEmpty(dtoLogin)) {
            return gson.toJson(new EmployerError("Bad token => " + dtoLogin));
        }
        if (daoEmployer.deleteAccordingToLogin(dtoLogin)) {
            return gson.toJson(new EmployerResp(dtoLogin, "OK"));
        }
        return gson.toJson(new EmployerError("Bad employee json => " + dtoLogin));
    }

    public String setVacancyActiveInactive(DtoSetVacancyActiveInactiveRequest dto) {
        try {
            daoVacancy.setVacancyActiveInactive(dto.getLogin(), dto.getVacancy());
            return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));
        } catch (NullPointerException | SQLException ex) {
            log.log(Level.SEVERE, ex.getMessage());
            return gson.toJson(new EmployerError("There are no user in dataBase with Id => " + dto.getLogin()));
        }
    }

    public boolean addVacancy(DtoRequestToAddVacancytoEmployer dto) throws EmployerDaoException, SQLException {
        if (StringUtils.isEmpty(dto.getLogin()) || StringUtils.isEmpty(dto.getPosition()) || dto.getSalary() < 0) {
            log.log(Level.WARNING, "Bad vacancy description or ID invalid");
            return false;
        }
        Vacancy vacancy = new Vacancy(dto.getPosition(), dto.getCompanyName(), dto.getSalary(), dto.isActive());
        daoVacancy.addVacancy(dto.getLogin(), vacancy);
        return true;

    }

    public String deleteVacancy(DtoForRemoveVacancyFromEmployer dto) throws SQLException {
        if (StringUtils.isEmpty(dto.getVacancy_name())) {
            log.log(Level.SEVERE, dto.getVacancy_name() + "=> vacancy is invalid");
            return gson.toJson(new EmployerError(dto.getVacancy_name() + "=> token or vacancy is invalid"));

        }
        if (daoVacancy.removeVacancy(dto.getVacancy_name())) {

            return gson.toJson(new EmployerResp(dto.getVacancy_name(), "OK"));
        }
        return gson.toJson(new EmployerError("There are no user with such ID => " + dto.getVacancy_name()));
    }

    public String changeFirstName(DtoChangeEmployerFirstName dto) throws EmployerDaoException, SQLException {
        try {
            daoEmployer.setNewFirstName(dto.getLogin(), dto.getFirstName());
            return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));
        } catch (NullPointerException ex) {
            return gson.toJson(new EmployerError(ex.getMessage()));
        }
    }

    public String changeEmail(DtoChangeEmployerEmaiReq dto) {

        try {
            daoEmployer.changeEmail(dto.getLogin(), dto.getEmail());
            return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));
        } catch (Exception e) {
            return gson.toJson(new EmployerError(e.getMessage()));
        }

    }

    public String changeSecondName(DtoChangeEmployerSecondName dto) throws EmployerDaoException, SQLException {
        try {
            daoEmployer.setNewSecondName(dto.getLogin(), dto.getSecondName());
            return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));

        } catch (NullPointerException ex) {
            return gson.toJson(new EmployerError(ex.getMessage()));
        }

    }

    public String changeFatherName(DtoChangeEmployerFatherName dto) throws EmployerDaoException, SQLException {
        try {
            daoEmployer.setNewFatherName(dto.getLogin(), dto.getFatherName());
            return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));

        } catch (IllegalArgumentException ex) {
            return gson.toJson(new EmployerError(ex.getMessage()));
        }
    }


    public String changeCompanyName(DtoChangeEmployerCompanyName dto) {
        try {
            daoEmployer.changeCompanyName(dto.getLogin(), dto.getCompanyName());
            return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));

        } catch (Exception e) {
            log.log(Level.SEVERE, dto.getLogin() + " is invalid");
            return gson.toJson(new EmployerError(e.getMessage()));
        }

    }

    public String changeAddress(DtoChangeEmployerAddressReq dto) throws SQLException {

        if (StringUtils.isEmpty(dto.getLogin()) || StringUtils.isEmpty(dto.getAddress())) {
            log.log(Level.SEVERE, dto.getLogin() + " is invalid or address =>" + dto.getAddress() + " is invalid");
            return gson.toJson(new EmployerError(dto.getLogin() + " is invalid or address =>" + dto.getAddress() + " is invalid"));
        }
        daoEmployer.changeAddress(dto.getLogin(), dto.getAddress());
        return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));
    }

    public String setProfileActiveInactive(DtoSetProfActiveInactive dto) {

        try {
            daoEmployer.setProfileActiveInactive(dto.getLogin());
            return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));
        } catch (Exception e) {
            return gson.toJson(new EmployerError(e.getMessage()));
        }

    }


    public String changePassword(DtoChangeEmployerPassReq dto) {
        if (StringUtils.isEmpty(dto.getLogin())) { // Добавить метод проверки пароля в CheckService
            log.log(Level.SEVERE, "invalid token =>'" + dto.getLogin() + "'");
            return gson.toJson(new EmployerError("token => '" + dto.getLogin() + "' is invalid"));
        }
        try {
            daoEmployer.changePassword(dto.getLogin(), dto.getPassword());
            return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));

        } catch (Exception e) {
            return gson.toJson(new EmployerError(e.getMessage()));
        }
    }

    public String changeDemandLevel(DtoReqToSetNEwLevelToDemand dto) {
        log.log(Level.WARNING, "If level user mentioned is less than 1 or more than" +
                " 5 that value will be changed into number which is closest to broad of levels");
        int level = dto.getDemandLevel() < 1 ? 1 : (dto.getDemandLevel() > 5 ? 5 : dto.getDemandLevel());
        try {
            daoDemand.changeVacancyDemandLevel(dto.getLogin(), dto.getVacancy(), dto.getDemandName(), level);
            return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));

        } catch (EmployerDaoException | EmployeeDaoException e) {
            return gson.toJson(new EmployerError(e.getMessage()));
        }
    }

    public String changeDemandIsNecessary(DtoReqToChangeNecessaryOofDemand dto) {
        String token = dto.getLogin();
        String demandName = dto.getDemandName();
        String vacancy = dto.getVacancy();
        try {
            daoDemand.changeVacancyDemandIsNecessary(token, vacancy, demandName);
            return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));
        } catch (EmployerDaoException e) {
            log.log(Level.SEVERE, "Please, check token, vacancy, name of demand");
            return gson.toJson(new EmployerError(e.getMessage()));
        }
    }


    public String getVacancies(DtoVacanciesRequestFOrEmployee dto) throws SQLException {
        if (StringUtils.isEmpty(dto.getLogin())) {
            return gson.toJson(new EmployerError("There are no user with ID = '" + dto.getLogin()));
        }
        return gson.toJson(new EmployerResp(dto.getLogin(), daoEmployer.getVacancies(dto.getLogin()).values().toString()));
    }

    public String getActiveVacancies(DtoVacanciesRequestFOrEmployee dto) throws SQLException {
        if (StringUtils.isEmpty(dto.getLogin())) {
            return gson.toJson(new EmployerError("There are no user with ID = " + dto.getLogin()));
        }
        return gson.toJson(new EmployerResp(dto.getLogin(), daoEmployer.getActiveVacancies(dto.getLogin()).toString()));
    }

    public String getInactiveVacancies(DtoVacanciesRequestFOrEmployee dto) throws SQLException {
        if (StringUtils.isEmpty(dto.getLogin())) {
            return gson.toJson(new EmployerError("There are no user with ID = " + dto.getLogin()));
        }
        return gson.toJson(new EmployerResp(dto.getLogin(), daoEmployer.getInactiveVacancies(dto.getLogin()).toString()));
    }

    public String changeVacancyDemandName(DtoChangeVacancyDemandName dto) {
        try {
            daoDemand.changeVacancyDemandName(dto.getLogin(), dto.getVacancy(), dto.getDemandName(), dto.getNewDemandName());
            return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));
        } catch (EmployerDaoException e) {
            log.log(Level.SEVERE, "No such vacancy or wrong demand name");
            return gson.toJson(new EmployerError(e.getMessage()));
        }

    }

    public String addDemandToVacancy(DtoAddDemandToVacancyReq dto) throws EmployerDaoException, SQLException {
        if (StringUtils.isEmpty(dto.getLogin()) || StringUtils.isEmpty(dto.getDemandName())
                || StringUtils.isEmpty(dto.getVacancy())) {
            log.log(Level.SEVERE, "No such vacancy or wrong demand description");
            return gson.toJson(new EmployerError(String.format("There can be bad one of: 1)%s,2)%s,3)%s",
                    dto.getDemandName(), dto.getLogin(), dto.getVacancy())));
        }
        Demand demand = new Demand(dto.getDemandName(), dto.getDemandLevel(), dto.isNecessary());
        if (demand.getLevel() < 1 || demand.getLevel() > 5) {
            log.log(Level.WARNING, "If level user mentioned is less than 1 or more than 5 that value will be changed into number which is closest to broad of levels");
        }
        demand.setLevel(demand.getLevel() < 1 ? 1 : (demand.getLevel() > 5 ? 5 : demand.getLevel()));
        if (daoDemand.addDemandToAVacancy(dto.getLogin(), dto.getVacancy(), demand)) {
            return gson.toJson(new EmployerResp(dto.getLogin(), "OK"));
        }
        return gson.toJson(new EmployerError("There are no user in database with ID =>" + dto.getLogin()));
    }


    public String getPotentialEmployeesHaveAllSkills(DtoForUsageOnleVacancy dto) {
        try {
            Set<Employee> set = daoEmployer.getPotentialEmployeesHaveAllSkills(dto.getLogin(), dto.getVacancy());
            return gson.toJson(new EmployerResp(dto.getLogin(), set.toString()));
        } catch (EmployerDaoException e) {
            return gson.toJson(new EmployerError(e.getMessage()));
        }
    }

    public String getPotentialEmployeeSetWithNecessarySkillsNotLessThanLevel(DtoForUsageOnleVacancy dto) {
        try {
            Set<Employee> set = daoEmployer.getPotentialEmployeeSetWithNecessarySkillsNotLessThanLevel(dto.getLogin(), dto.getVacancy());
            return gson.toJson(new EmployerResp(dto.getLogin(), set.toString()));

        } catch (EmployerDaoException e) {
            return gson.toJson(new EmployerError(e.getMessage()));
        }

    }

    public String getPotentialEmployeesHaveAllNeededForVacancy(DtoForUsageOnleVacancy dto) {
        try {
            Set<Employee> set = daoEmployer.getPotentialEmployeesHaveAllNeededForVacancy(dto.getLogin(), dto.getVacancy());
            return gson.toJson(new EmployerResp(dto.getLogin(), set.toString()));

        } catch (EmployerDaoException e) {
            return gson.toJson(new EmployerError(e.getMessage()));
        }

    }

    public String getSetOfEmployeeHasOneNeededSkillNotLessThanLevel(DtoForUsageOnleVacancy dto) {
        try {
            Set<Employee> set = daoEmployer.getSetOfEmployeeHasOneNeededSkillNotLessThanLevel(dto.getLogin(), dto.getVacancy());
            return gson.toJson(new EmployerResp(dto.getLogin(), set.toString()));

        } catch (EmployerDaoException e) {
            return gson.toJson(new EmployerError(e.getMessage()));
        }

    }

}
