package hiring.service.userservice.employeeservice;

import com.google.gson.Gson;
import hiring.dtoobjects.dtoclients.dtoemployee.DtoEmployee;
import hiring.clients.employee.Employee;
import hiring.clients.employee.daoemployee.DaoEmployee;
import hiring.clients.employee.daoemployee.daoexceptions.EmployeeDaoException;
import hiring.demand.Demand;
import hiring.dtorequests.DtoParent;
import hiring.dtorequests.employeeresponse.DtoAddSkillToEmployeeRequest;
import hiring.dtorequests.employeeresponse.DtoDeleteEmployeeSkillRequest;
import hiring.dtorequests.employeeresponse.DtoNewSkillLevelRequest;
import hiring.dtorequests.employeeresponse.DtoSetVacancyToEmployee;
import hiring.errors.errorofemployee.EmployeeError;
import hiring.jsonresponces.employeeresponce.EmployeeResponse;
import hiring.vacancy.Vacancy;
import hiring.vacancy.daovacancy.DaoVacancy;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EmployeeService {

    private DaoVacancy daoVacancy;
    private static Gson gson = new Gson();


    private DaoEmployee daoEmployee;
    private Gson GSON = new Gson();
    private Logger log = Logger.getLogger(EmployeeService.class.getName());

    public EmployeeService(DaoEmployee daoEmployee, DaoVacancy daoVacancy) {
        this.daoEmployee = daoEmployee;
        this.daoVacancy = daoVacancy;
    }

    public String registerEmployee(String jsonEmployee) throws SQLException {

        Employee employee = GSON.fromJson(jsonEmployee, Employee.class);
        employee.setSkills(new HashMap<>());
        employee.setToken(UUID.randomUUID().toString());
        if (!daoEmployee.insert(employee)) {
            return gson.toJson(new EmployeeError("There are one employee with same login."));
        }
        return gson.toJson(new EmployeeResponse(employee.getToken(), "OK"));
    }

    public String deleteEmployee(String login) throws EmployeeDaoException, SQLException {
        if (daoEmployee.delete(login)) {
            return gson.toJson(new EmployeeResponse(login, "OK"));
        }
        return gson.toJson(new EmployeeError("Not an employee description"));


    }

    public String changePassword(String login, String pass) throws SQLException {
        if (daoEmployee.changePassword(login, pass)) {
            return gson.toJson(new EmployeeResponse(login, "OK"));
        }
        log.log(Level.SEVERE, "Please check login, old password, new password:" +
                " login might be unavailable, new password might be not so strong");
        return gson.toJson(new EmployeeError("Please check new password"));

    }

    public String deleteSkill(DtoDeleteEmployeeSkillRequest dto) {
        if (daoEmployee.deleteSkill(dto.getToken(), dto.getSkillName())) {
            return gson.toJson(new EmployeeResponse(dto.getToken(), "OK"));
        }
        log.log(Level.SEVERE, dto.getToken() + " is invalid");
        return gson.toJson(new EmployeeError("json is invalid"));

    }

    public String changeEmployeeFirstName(String token, String newFirstName) throws SQLException {
        if (daoEmployee.changeFirstName(token, newFirstName)) {
            return gson.toJson(new EmployeeResponse(token, "OK"));
        }
        log.log(Level.SEVERE, newFirstName + " is invalid or " + token + " is invalid");
        return gson.toJson("newFirstName + \" is invalid or \" + jsonToken + \" is invalid\"");
    }

    public String addSkill(DtoAddSkillToEmployeeRequest dto) throws EmployeeDaoException, SQLException {
        if (StringUtils.isEmpty(dto.getLogin()) || StringUtils.isEmpty(dto.getDemandName())) {
            log.log(Level.SEVERE, "Skill description is empty or " + dto.getLogin() + " is invalid");
            return gson.toJson(new EmployeeError("Bad token => " + dto.getLogin() + "or no skill mentioned"));
        }
        Demand demand = new Demand(dto.getDemandName(), dto.getLevel());
        demand.setNecessary(true);
        daoEmployee.addSkill(dto.getLogin(), demand);
        return gson.toJson(new EmployeeResponse(dto.getLogin(), "OK"));
    }

    public String setEmployeeNewLevelOfSkill(DtoNewSkillLevelRequest dto) {
        if (daoEmployee.setNewLevelToSkill(dto.getLogin(), dto.getDemandName(), dto.getLevel())) {
            return gson.toJson(new EmployeeResponse(dto.getLogin(), "OK"));

        }
        log.log(Level.SEVERE, "login is empty or skill name is empty");
        return gson.toJson(new EmployeeError("Bad token => " + dto.getLogin()));

    }


    public String changeEmployeeSecondName(DtoEmployee dto) throws SQLException {
        if (daoEmployee.changeSecondName(dto.getToken(), dto.getSecondName())) {
            return gson.toJson(new EmployeeResponse(dto.getToken(), "OK"));
        }
        return gson.toJson(new EmployeeError("Bad token => " + dto.getToken()));
    }

    public String changeFatherName(DtoEmployee dtoEmployee) throws SQLException {
        if (daoEmployee.changeFatherName(dtoEmployee.getToken(), dtoEmployee.getFatherName())) {
            return gson.toJson(new EmployeeResponse(dtoEmployee.getToken(), "OK"));
        }
        log.log(Level.SEVERE, "Bad token => " + dtoEmployee.getToken());
        return gson.toJson(new EmployeeError("Bad token => " + dtoEmployee.getToken()));
    }


    public String setVacancyAndGoToInactiveEmployees(DtoSetVacancyToEmployee dto) throws EmployeeDaoException {
//        String employerToken = dto.getEmployerToken();
//
//        String employeeToken = dto.getEmployeeToken();
//        String vacancy = dto.getVacancy();
//        String company = dto.getCompanyName();
//
//        if (StringUtils.isEmpty(employeeToken)
//                || StringUtils.isEmpty(company) || StringUtils.isEmpty(vacancy)
//                || StringUtils.isEmpty(employerToken)) {
//            log.log(Level.SEVERE, String.format("Bad one of: 1)%s, 2)%s, 3)%s, 4)%s",
//                    employeeToken, employerToken, vacancy, company));
//
//            return gson.toJson(new EmployeeError("Bad tokens or company name or vacancy name"));
//        }
//        daoEmployee.setVacancyAndGoToInactive(employeeToken, employerToken, company, vacancy);
//        daoVacancy.setVacancyActiveInactive(employerToken, vacancy);
//        return gson.toJson(new EmployeeResponse(dto.getEmployeeToken(), "OK"));

        return "";
    }

    public String getVacanciesMostSuitableForEmployee(DtoParent dto) {
//        try {
//            Set<Vacancy> set = daoEmployee.getVacanciesMostSuitable(dto.getToken());
//            return gson.toJson(new EmployeeResponse(dto.getToken(), set.toString()));
//        } catch (Exception e) {
//            return gson.toJson(new EmployeeError(e.getMessage()));
//        }
        return "";

    }

    public String getNecessaryVacanciesNotLessThanEmployeeLevel(DtoParent dto) {

//        try {
//            Set<Vacancy> set = daoEmployee.getNecessaryVacanciesNotLessThanEmployeeLevel(dto.getToken());
//            return gson.toJson(new EmployeeResponse(dto.getToken(), set.toString()));
//
//        } catch (EmployeeDaoException e) {
//            return gson.toJson(new EmployeeError(e.getMessage()));
//        }
        return "";

    }

    public String getVacanciesAllLevel(DtoParent dto) {
//        try {
//            Set<Vacancy> set = daoEmployee.getVacanciesAllLevel(dto.getToken());
//            return gson.toJson(new EmployeeResponse(dto.getToken(), set.toString()));
//
//        } catch (EmployeeDaoException e) {
//            return gson.toJson(new EmployeeError(e.getMessage()));
//        }
        return "";
    }


    public String getVacanciesSuitableOneDemandNotLessThanLevel(DtoParent dto) {

//        try {
//            Set<Vacancy> set = daoEmployee.getVacanciesSuitableOneDemandNotLessThanLevel(dto.getToken());
//            return gson.toJson(new EmployeeResponse(dto.getToken(), set.toString()));
//
//        } catch (EmployeeDaoException e) {
//            return gson.toJson(new EmployeeError(e.getMessage()));
//        }
        return "";
    }
}
