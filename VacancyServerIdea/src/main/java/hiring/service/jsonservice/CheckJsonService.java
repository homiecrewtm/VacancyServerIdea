package hiring.service.jsonservice;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import hiring.dtoobjects.dtoclients.dtoemployee.DtoEmployee;
import hiring.clients.employer.Employer;
import hiring.demand.Demand;
import hiring.vacancy.Vacancy;
import org.apache.commons.lang3.StringUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class CheckJsonService {

    private static Gson gson = new Gson();
    private static Logger log = Logger.getLogger(CheckJsonService.class.getName());

    public static boolean checkJsonIfDemand(String json) {
        if (isJsonValid(json)) {
            Demand demand = gson.fromJson(json, Demand.class);
            boolean a = !StringUtils.isEmpty(demand.getDemandName());
            boolean b = StringUtils.contains(json, "'level':");
            return a && b;
        }
        log.log(Level.SEVERE, "json is not valid, please check out your json and try again. See docs for Gson.");
        return false;
    }


    public static boolean checkTokenJson(String json) {
        return json.length() >= 36 && isJsonValid(json);

    }


    public static boolean checkIfEmployee(String jsonEmployee) {
        if (isJsonValid(jsonEmployee)) {
            DtoEmployee employee = gson.fromJson(jsonEmployee, DtoEmployee.class);
            boolean a = !StringUtils.isEmpty(employee.getFirstName());
            boolean b = !StringUtils.isEmpty(employee.getSecondName());
            boolean c = !StringUtils.isEmpty(employee.getLogin());
            boolean d = !StringUtils.isEmpty(employee.getPassword());
            boolean e = !StringUtils.isEmpty(employee.getEmail());
            if (!a || !b || !c || !d || !e) {
                log.log(Level.WARNING, "Check parameters you give to register method - you may not point out all parameters needed for registration employee user.");
            }
            return a && b && c && d && e;

        }
        return false;
    }

    public static boolean checkJsonIfEmployer(String jsonEmployer) {

        if (isJsonValid(jsonEmployer)) {
            Employer employer = gson.fromJson(jsonEmployer, Employer.class);
            boolean a = !StringUtils.isEmpty(employer.getCompanyName());
            boolean b = !StringUtils.isEmpty(employer.getAddress());
            boolean d = !StringUtils.isEmpty(employer.getFirstName());
            boolean e = !StringUtils.isEmpty(employer.getSecondName());
            boolean f = !StringUtils.isEmpty(employer.getLogin());
            boolean g = !StringUtils.isEmpty(employer.getPassword());
            return a && b && d && e && f && g;

        }
        log.log(Level.WARNING, "json is not valid, please check out your json and try again. See docs for Gson.");

        return false;
    }

    public static boolean checkJsonIfVacancy(String jsonVacancy) {
        if (isJsonValid(jsonVacancy)) {
            Vacancy vacancy = gson.fromJson(jsonVacancy, Vacancy.class);
            boolean b1 = !StringUtils.isEmpty(vacancy.getPosition());
            boolean b2 = StringUtils.contains(jsonVacancy, "'salary':");
            return b1 && b2;
        }
        log.log(Level.WARNING, "json is not valid, please check out your json and try again. See docs for Gson.");

        return false;
    }

    public static boolean isJsonValid(String json) {
        try {
            if (json.equals("") || json.equals("{}")) {
                throw new JsonSyntaxException("'" + json + "' is not valid, please check out your json and try again. See docs for Gson.");
            }
            new Gson().fromJson(json, Object.class);
            return true;
        } catch (JsonSyntaxException e) {
            log.log(Level.SEVERE, e.getMessage());
            return false;
        }
    }


}
