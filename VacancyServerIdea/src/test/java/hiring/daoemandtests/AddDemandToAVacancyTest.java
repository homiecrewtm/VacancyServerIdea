package hiring.daoemandtests;

import hiring.clients.employer.daoexceptions.EmployerDaoException;
import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;

public class AddDemandToAVacancyTest {

    @Test
    public void name() throws EmployerDaoException, SQLException, DbException {

        Server server = Server.startServer();
        server.clear();
        server.registerEmployer("{'firstName':'FirstName','secondName':'SecondName','fatherName':''," +
                "'password':'12345','login':'LolFChiken','companyName':'CompanuName - Euro','address':'Address - USA','email':'USA@gmail.com'}");

        server.addVacancyToEmployer("{'login':'LolFChiken','position':'worker','salary':20000,'companyName':'Nestle'}");

        server.addDemandToVacancyOfEmployer("{'login':'LolFChiken','vacancy':'worker','demandName':'Java','demandLevel':4,'isNecessary':'true'}");

    }
}
