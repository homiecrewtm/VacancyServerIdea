package hiring.employeetests;

import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;

public class ChangeEmployeePasswordTest {
    @Test
    public void name() {
        try {
            Server server = Server.startServer();
            server.clear();

            server.registerEmployee("{'firstName':'Gf','secondName':'Kf'," +
                    "'fatherName':'','login':'employee_keke','password':'12345'," +
                    "'email':'user@gmail.com','isActive':'true','skill_id':''}");

            server.changeUserPassword("{'login':'employee_keke','password':'1234'}");
            int hash = "1234".hashCode();
            assertEquals(hash, server.getDB().getStatement()
                    .executeQuery("select password from secret_data where login='employee_keke'")
                    .getInt("password"));
        } catch (SQLException | NullPointerException | DbException e) {
            System.out.println(e.getMessage());
        }
    }
}
