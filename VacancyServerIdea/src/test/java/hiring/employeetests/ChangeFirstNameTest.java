package hiring.employeetests;

import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.SQLException;

public class ChangeFirstNameTest {
    @Test
    public void name() {

        try {
            Server server = Server.startServer();
            server.clear();
            server.registerEmployee("{'firstName':'Gf','secondName':'Kf'," +
                    "'fatherName':'','login':'employee_keke','password':'12345'," +
                    "'email':'user@gmail.com','isActive':'true'}");

            server.changeUserFirstName("{'login':'employee_keke','firstName':'kagor'}");
        } catch (SQLException | NullPointerException e) {
            System.out.println(e.getMessage());
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

}
