package hiring.employeetests;

import hiring.clients.employee.daoemployee.daoexceptions.EmployeeDaoException;
import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;

public class addSkillTest {
    @Test
    public void name() throws DbException, SQLException, InterruptedException {


        try {
            Server server = Server.startServer();
            server.clear();
            server.registerEmployee("{'firstName':'Gf','secondName':'Kf'," +
                    "'fatherName':'','login':'employee_keke','password':'12345'," +
                    "'email':'user@gmail.com','isActive':'true'}");
            server.addSkillToEmployee("{'login':'employee_keke','skillName':'Java','level':3}");
            String a = server.getDB().getStatement().executeQuery("select skill_id from skills where name='Java';").getString("skill_id");
            String b = server.getDB().getStatement().executeQuery("select skill_id from employees where login='employee_keke';").getString("skill_id");
            assertEquals(a, b);
        } catch (SQLException | NullPointerException e) {
            System.out.println(e.getMessage());
        } catch (EmployeeDaoException e) {
            e.printStackTrace();
        }

    }
}
