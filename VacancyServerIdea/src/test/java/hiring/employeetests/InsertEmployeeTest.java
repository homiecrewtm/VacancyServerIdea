package hiring.employeetests;

import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;

public class InsertEmployeeTest {
    @Test
    public void name() {
        try {

            Server server = Server.startServer();
            server.clear();

            server.registerEmployee("{'firstName':'Gf','secondName':'Kf','fatherName':'','login':'employee_keke','password':'12345','email':'user@gmail.com','isActive':'true'}");
            PreparedStatement ps = server.getDB().getConnection().prepareStatement("select firstName from users where login=?");
            ps.setString(1, "employee_keke");
            String normName = ps.executeQuery().getString("firstName");
            assertEquals(normName, "Gf");
        } catch (SQLException | NullPointerException e) {
            System.out.println(e.getMessage());
        } catch (DbException e) {
            e.printStackTrace();
        }


    }
}
