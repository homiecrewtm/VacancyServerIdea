package hiring.employertests;

import hiring.clients.employer.daoexceptions.EmployerDaoException;
import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SetNewFirstNameTest {
    @Test
    public void name() {

        try {
            Server server = Server.startServer();
            server.clear();

            server.registerEmployer("{'firstName':'Gf','secondName':'Kf','fatherName':''," +
                    "'password':'12345','login':'LolFChiken','companyName':'Euro','address':'USA','email':'USA@gmail.com'}");
            String a = server.getDB().getStatement().executeQuery("select  * from users").getString("firstName");
            assertEquals(a,"Gf");
            server.changeEmployerFirstName("{'login':'LolFChiken','firstName':'Drogo'}");
            String b = server.getDB().getStatement().executeQuery("select  * from users").getString("firstName");
            assertEquals(b,"Drogo");
        } catch (NullPointerException | SQLException | EmployerDaoException | DbException e) {
            e.printStackTrace();
        }
    }
}
