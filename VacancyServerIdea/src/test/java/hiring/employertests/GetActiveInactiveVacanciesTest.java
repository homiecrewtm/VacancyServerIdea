package hiring.employertests;

import hiring.clients.employer.daoexceptions.EmployerDaoException;
import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.SQLException;

import static junit.framework.TestCase.assertTrue;

public class GetActiveInactiveVacanciesTest {

    @Test
    public void name() {
        try {
            Server server = Server.startServer();
            server.clear();

            server.registerEmployer("{'firstName':'Gf','secondName':'Kf','fatherName':''," +
                    "'password':'12345','login':'LolFChiken','companyName':'Euro','email':'keke@gmail.com','address':'USA'}");

            String loginJson = "{'login':'LolFChiken'}";

            server.addVacancyToEmployer("{'login':'LolFChiken','position':'kek1','salary':20000,'companyName':'Nestle','isActive':'false'}");
            server.addVacancyToEmployer("{'login':'LolFChiken','position':'kek2','salary':20000,'companyName':'Nestle','isActive':'true'}");
            server.addVacancyToEmployer("{'login':'LolFChiken','position':'kek3','salary':20000,'companyName':'Nestle','isActive':'true'}");
            server.addVacancyToEmployer("{'login':'LolFChiken','position':'kek4','salary':20000,'companyName':'Nestle','isActive':'true'}");

            String a = server.getEmployerActiveVacancies(loginJson);
            assertTrue(a.contains("kek2") && a.contains("kek3") && a.contains("kek4"));

            String b = server.getEmployerInactiveVacancies(loginJson);
            assertTrue(b.contains("kek1"));
        } catch (NullPointerException | SQLException | DbException | EmployerDaoException e) {
            e.printStackTrace();
        }
    }
}
