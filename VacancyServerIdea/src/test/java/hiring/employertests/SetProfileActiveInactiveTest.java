package hiring.employertests;

import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.*;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class SetProfileActiveInactiveTest {
    @Test
    public void name() throws DbException {
        try {

            Server server = Server.startServer();
            server.clear();
            server.registerEmployer("{'firstName':'Gf','secondName':'Kf','fatherName':''," +
                    "'password':'12345','login':'login','companyName':'Euro','email':'keke@gmail.com','address':'USA'}");
            server.setProfileOfEmployerActiveInactive("{'login':'login'}");

            boolean b = server.getDB().getConnection().createStatement().executeQuery("select isActive from users where login='login';").getBoolean("isActive");
            assertFalse(b);
            server.setProfileOfEmployerActiveInactive("{'login':'login'}");
            b = server.getDB().getConnection().createStatement().executeQuery("select isActive from users where login='login';").getBoolean("isActive");
            assertTrue(b);
        } catch (NullPointerException | SQLException e) {
            e.printStackTrace();
        }
    }
}
