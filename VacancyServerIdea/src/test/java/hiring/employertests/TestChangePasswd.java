package hiring.employertests;

import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestChangePasswd {


    @Test
    public void testChangePasswd() {
        try {
            Server server = Server.startServer();
            server.clear();
            server.registerEmployer("{'firstName':'Gf','secondName':'Kf','fatherName':''," +
                    "'password':'12345','login':'LolFChiken','companyName':'Euro','address':'USA','email':'USA@gmail.com'}");
            ResultSet _rs = server.getDB().getStatement().executeQuery("select * from secret_data");
            assertEquals(_rs.getString("password"), String.valueOf("12345".hashCode()));
            server.changeEmployerPassword("{'login':'LolFChiken','password':'Pipi'}");
            System.out.println("Pipi".hashCode());
            ResultSet rs = server.getDB().getStatement().executeQuery("select * from secret_data");
            assertNotEquals(Long.toString(rs.getLong(3)), String.valueOf("12345".hashCode()));
            assertEquals(Long.toString(rs.getLong(3)), String.valueOf("Pipi".hashCode()));
        } catch (NullPointerException | SQLException | DbException e) {
            e.printStackTrace();
        }
    }
}
