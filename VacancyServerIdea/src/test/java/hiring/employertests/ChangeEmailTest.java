package hiring.employertests;

import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;

public class ChangeEmailTest {
    @Test
    public void name() {
        try {
            Server server = Server.startServer();
            server.clear();

            server.registerEmployer("{'firstName':'Gf','secondName':'Kf','fatherName':''," +
                    "'password':'12345','login':'LolFChiken','companyName':'Euro','email':'keke@gmail.com','address':'USA'}");
            String a = server.getDB().getStatement().executeQuery("select  * from users").getString("email");
            assertEquals(a, "keke@gmail.com");
            server.changeEmployerEmail("{'login':'LolFChiken','email':'me@gmail.com'}");
            a = server.getDB().getStatement().executeQuery("select  * from users").getString("email");
            assertEquals(a, "me@gmail.com");

        } catch (NullPointerException | SQLException | DbException e) {
            e.printStackTrace();
        }
    }
}
