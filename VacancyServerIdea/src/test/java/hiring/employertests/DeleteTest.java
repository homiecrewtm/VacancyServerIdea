package hiring.employertests;

import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.SQLException;

import static jdk.nashorn.internal.runtime.regexp.joni.Config.log;

public class DeleteTest {


    @Test
    public void name() {

        try {

            Server server = Server.startServer();
            server.clear();

            server.registerEmployer("{'firstName':'Gf','secondName':'Kf','fatherName':''," +
                    "'password':'12345','login':'LolFChiken','companyName':'Euro','address':'USA','email':'USA@gmail.com'}");

            server.deleteEmployerAccordingToLogin("{'login':'LolFChiken'}");

        } catch (SQLException | NullPointerException e) {
            log.println(e.getMessage());
        } catch (DbException e) {
            e.printStackTrace();
        }
    }
}
