package hiring.employertests;

import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ChangeCompanyNameTest {

    @Test
    public void name() {

        try {
            Server server = Server.startServer();
            server.clear();

            server.registerEmployer("{'firstName':'Gf','secondName':'Kf','fatherName':''," +
                    "'password':'12345','login':'LolFChiken','companyName':'Euro','address':'USA','email':'USA@gmail.com'}");
            String s = server.getDB().getStatement().executeQuery("select  * from employers").getString("companyName");
            assertEquals(s, "Euro");
            server.changeEmployerCompanyName("{'login':'LolFChiken','companyName':'Apple'}");
            String f = server.getDB().getStatement().executeQuery("select  * from employers").getString("companyName");
            assertEquals(f, "Apple");
        } catch (NullPointerException | SQLException | DbException e) {
            e.printStackTrace();
        }

    }
}
