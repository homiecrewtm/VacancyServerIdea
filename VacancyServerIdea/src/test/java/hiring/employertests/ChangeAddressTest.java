package hiring.employertests;

import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;

public class ChangeAddressTest {

    @Test
    public void name() {
        try {
            Server server = Server.startServer();
            server.clear();

            server.registerEmployer("{'firstName':'Gf','secondName':'Kf','fatherName':''," +
                    "'password':'12345','login':'LolFChiken','companyName':'Euro','address':'USA','email':'USA@gmail.com'}");
            String d = server.getDB().getStatement().executeQuery("select * from employers").getString("address");
            assertEquals(d,"USA");
            server.changeEmployerAddress("{'login':'LolFChiken','address':'Sunny Beach'}");
            String l = server.getDB().getStatement().executeQuery("select * from employers").getString("address");
            assertEquals(l,"Sunny Beach");
        } catch (NullPointerException | SQLException | DbException e) {
            e.printStackTrace();
        }
    }
}
