package hiring.employertests;

import hiring.clients.employer.daoexceptions.EmployerDaoException;
import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.SQLException;

public class SetNewSecondNameTest {
    @Test
    public void name() {
        try {
            Server server = Server.startServer();
            server.clear();

            server.registerEmployer("{'firstName':'Gf','secondName':'Kf','fatherName':''," +
                    "'password':'12345','login':'LolFChiken','companyName':'Euro','email':'keke@gmail.com','address':'USA'}");



            server.changeEmployerSecondName("{'login':'LolFChiken','secondName':'Valera'}");


        } catch (NullPointerException | SQLException | EmployerDaoException | DbException e) {
            e.printStackTrace();
        }
    }
}
