package hiring.employertests;

import hiring.clients.employer.daoexceptions.EmployerDaoException;
import hiring.db.dbexceptions.DbException;
import hiring.server.Server;
import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;

public class AddVacancyRemoveVacancyTest {


    @Test
    public void name() throws EmployerDaoException, DbException, SQLException {
        Server server = Server.startServer();
        server.clear();
        server.registerEmployer("{'firstName':'FirstName','secondName':'SecondName','fatherName':''," +
                "'password':'12345','login':'LolFChiken','companyName':'CompanuName - Euro','address':'Address - USA','email':'USA@gmail.com'}");

        server.addVacancyToEmployer("{'login':'LolFChiken','position':'worker','salary':20000,'companyName':'Nestle'}");


        PreparedStatement ps2 = server.getDB().getConnection().prepareStatement("select employer_token from vacancies where name=?");
        PreparedStatement ps3 = server.getDB().getConnection().prepareStatement("select employer_token from employers where login=?");

        ps2.setString(1, "worker");
        ps3.setString(1, "LolFChiken");
        String a = ps2.executeQuery().getString("employer_token");
        String b = ps2.executeQuery().getString("employer_token");
        assertEquals(a, b);

        server.removeVacancyFromEmployer("{'vacancy_name':'worker'}");



    }
}
